#!/bin/bash

# on récupère le status de la requête qui test la connexion à l'api
response=$(curl --write-out '%{http_code}' --silent --output /dev/null http://127.0.0.1:5000/api)

if [ $response != 200 ]; then
    echo "> Serveur NodeJS non lancé"
    exit 1;
fi

# lancement du premier test
echo "\n##################################################################"
echo "                 LANCEMENT PREMIER TEST                           "
echo "##################################################################\n"
artillery run --quiet --output ./node_tests/abonnements_report.json ./node_tests/abonnements.yml
# création du rapport HTML
artillery report --output ./node_tests/abonnements_report.html ./node_tests/abonnements_report.json

# on attend 13min le temps que le test se termine
sleep 13m

# lancement du second test
echo "\n##################################################################"
echo "                  LANCEMENT SECOND TEST                           "
echo "##################################################################\n"
artillery run --quiet --output ./dist/test/recuperer_profil_report.json ./dist/test/recuperer_profil.yml

# # on attend 13min le temps que le test se termine
# sleep 13m

# # lancement du dernier test
# echo "##################################################################"
# echo "                 LANCEMENT DERNIER TEST                           "
# echo "##################################################################"
# artillery run --quiet --output ./dist/test/abonnements_report.json ./dist/test/abonnements.yml

echo "\n##################################################################"
echo "                         TESTS TEMRINÉS                           "
echo "##################################################################\n"