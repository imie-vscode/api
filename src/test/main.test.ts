require("dotenv").config();
import { expect } from "chai";
import { connectDb } from "../database";
import Socket from "../Socket";
import http from "http";
import express, { Application } from "express";
import { connectGMAIL } from "../utils/email";
import User from "../models/User";
import bcrypt from "bcrypt";
import { createTokenTest, getTokenTest } from "../utils/token";
import { isBirthday, isEmail, isExisted, isPassword, isPostalCode } from "../utils/validators";
import { formatPrice } from "../utils/formators";
import { createSubscription, removeSubscription } from "../utils/stripe";
import PdfGenerator from "../utils/PdfGenerator";
const PORT = process.env.PORT || 5000;

const app: Application = express();
const httpServer = http.createServer(app);

let server = null as http.Server;
let user = null;
let token = null;
let tokenDecoded = null;
let idStripe = null;

const FAKE_USER = {
  _id: null,
  email: "test@test.test",
  password: "",
  firstname: "Firstname",
  lastname: "Lastname",
  role: "User",
  city: "Saint-Jean du Cardonnay",
  address: "rue de l'école",
  numAddress: "16",
  postalCode: "76150",
  country: "France",
  pseudo: "ADR",
  sexe: "Homme",
  birthday: new Date("1997-05-23"),
};

const FAKE_CREDIT_CARD = {
  cardNumber: "4242424242424242",
  cvc: "123",
  exp_month: "12",
  exp_year: "2022",
};

const SUBSCRIPTION_NAME = "Classique";

const PASSWORD = "Azertyuiop123!";

// ###########################################################################################################################
//                                                      CONNEXIONS
// ###########################################################################################################################
describe("Initialisation", () => {
  it("Connexion à la base de données", async () => {
    let connected = false;
    try {
      await connectDb();
      connected = true;
    } catch (error) {
      connected = false;
    }
    expect(connected).to.be.true;
  });

  it("Connexion au serveur", async () => {
    let connected = false;
    try {
      server = httpServer.listen(PORT);
      connected = true;
    } catch (error) {
      console.log("error: ", error);
      connected = false;
    }
    expect(connected).to.be.true;
  });

  it("Connexion à Socket.IO", async () => {
    let connected = false;
    try {
      Socket(server);
      connected = true;
    } catch (error) {
      console.log("error: ", error);
      connected = false;
    }
    expect(connected).to.be.true;
  });

  it("Connexion à Nodemailer", async () => {
    let connected = false;
    try {
      await connectGMAIL();
      connected = true;
    } catch (error) {
      console.log("error: ", error);
      connected = false;
    }
    expect(connected).to.be.true;
  });
});

// ###########################################################################################################################
//                                                    VÉRIFICATIONS
// ###########################################################################################################################
describe("Vérification des champs", () => {
  it("Vérification code postal", () => {
    expect(isPostalCode(FAKE_USER.postalCode)).to.be.true;
  });

  it("Échec de la vérification du mot de passe", () => {
    expect(isPassword("azertyuiop123")).to.be.false;
  });

  it("Vérification de la date de naissance", () => {
    expect(isBirthday("1997-05-23")).to.be.true;
  });

  it("Échec de la vérification du courriel", () => {
    expect(isEmail("test@@test.com")).to.be.false;
    expect(isEmail("test@test..com")).to.be.false;
    expect(isEmail("testtest.com")).to.be.false;
  });

  it("Échec de la vérification qu'un champ existe", () => {
    expect(isExisted("")).to.be.false;
    expect(isExisted(null)).to.be.false;
    expect(isExisted(undefined)).to.be.false;
  });
});

// ###########################################################################################################################
//                                                      UTILISATEUR
// ###########################################################################################################################
describe("Insertion d'un utilisateur en base de données", () => {
  it("Génération du mot de passe", () => {
    const hash = bcrypt.hashSync(PASSWORD, 10);
    expect(hash).to.be.string;
    FAKE_USER.password = hash;
  });

  it("Création d'un utilisateur", async () => {
    try {
      const data = JSON.parse(JSON.stringify(FAKE_USER));
      delete data._id;
      user = await new User(data).save();
      FAKE_USER._id = user._id;
    } catch (error) {
      console.log("error: ", error);
    }
    expect(user?.email).to.be.equal(FAKE_USER.email);
  });
});

// ###########################################################################################################################
//                                                      TOKEN
// ###########################################################################################################################
describe("Gestion des tokens", () => {
  it("Création d'un token", () => {
    try {
      token = createTokenTest(user);
    } catch (error) {
      console.log("error: ", error);
    }
    expect(token).to.be.string;
  });

  it("Récupération d'un token", () => {
    try {
      tokenDecoded = getTokenTest(`Bearer ${token}`);
    } catch (error) {
      console.log("error: ", error);
    }
    expect(tokenDecoded?.email).to.be.equal(FAKE_USER.email);
  });
});

// ###########################################################################################################################
//                                                      PRIX
// ###########################################################################################################################
describe("Prix", () => {
  it("Formatter un nombre en prix", () => {
    const price = formatPrice(1000);
    expect(price).not.equal("1000");
  });
});

// ###########################################################################################################################
//                                                      STRIPE
// ###########################################################################################################################
describe("Paiement par Stripe", () => {
  it("Création d'un abonnement", async () => {
    const subscription = await createSubscription(FAKE_USER.email, FAKE_CREDIT_CARD, SUBSCRIPTION_NAME);
    idStripe = subscription.id;
    expect(subscription.status).to.be.equal("active");
  }).timeout(10000);
  it("Suppression d'un abonnement", async () => {
    const subscription = await removeSubscription(idStripe);
    expect(subscription.status).to.be.equal("canceled");
  }).timeout(10000);
});

// ###########################################################################################################################
//                                              CRÉATION D'UN PDF
// ###########################################################################################################################
describe("Création d'un PDF", () => {
  it("Génération du PDF", async () => {
    try {
      // on génère la facture
      const pdfGenerator = new PdfGenerator({}, "src/utils/pdf-templates/FactureTemplate.html", "", true);
      // on transforme le pdf en base64
      const buffer = (await pdfGenerator.generate()) as Buffer;
      expect(buffer?.toString("utf8")).to.be.string;
    } catch (error) {
      console.log("error: ", error);
    }
  });
}).timeout(5000);

// ###########################################################################################################################
//                                              CRÉATION D'UN PROJET
// ###########################################################################################################################
// describe("Création d'un projet", () => {
//   const projectPath = `projects/${FAKE_USER._id}`;
//   it("React", done => {
//     try {
//       const name = "test react";
//       const command = createReactApp(name, projectPath);

//       terminal.stdin.write(command);
//       terminal.stdin.end();
//       // évènement d'écoute de fin d'exécution du terminal
//       terminal.on("exit", code => {
//         expect(code).to.be.equal(1);
//         done();
//       });
//     } catch (error) {
//       console.log("error: ", error);
//     }
//   });
// });

// ###########################################################################################################################
//                                                      FIN
// ###########################################################################################################################
describe("Fermeture du serveur", () => {
  it("Suppression d'un utilisateur", async () => {
    try {
      user = await User.findById(FAKE_USER._id).deleteOne();
    } catch (error) {}
    expect(user?.deletedCount).to.be.equal(1);
  });

  it("Fermeture", () => {
    let closed = false;
    server.close(err => {
      closed = err ? false : true;
      expect(closed).to.be.true;
    });
  });
});
