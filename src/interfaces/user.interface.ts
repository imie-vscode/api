export interface IUpdateUserRequestBody {
  firstname: string;
  lastname: string;
  pseudo: string;
  address: string;
  num_address: string;
  city: string;
  postal_code: string;
  country: string;
  sexe: string;
  birthday: string;
  phone_number?: string;
}

export interface IUpdateUserSubscriptionRequestBody {
  subscription_id: string;
  card_number: string;
  cvc: string;
  exp: string;
}

export interface INewProjectRequestBody {
  name: string;
  template: "REACT" | "ANGULAR" | "VUE";
}

export interface IAddUserInProjectRequestBody {
  id_user: string;
}
