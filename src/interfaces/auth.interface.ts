export interface IRegisterRequestBody {
  email: string;
  firstname: string;
  lastname: string;
  pseudo: string;
  password: string;
  confirm_password: string;
  address: string;
  num_address: string;
  postal_code: string;
  country: string;
  city: string;
  sexe: string;
  birthday: string;
  newsletter?: boolean;
}

export interface ILoginRequestBody {
  email: string;
  password: string;
}

export interface IForgotPasswordRequestBody {
  email: string;
}

export interface IResetPasswordRequestBody {
  password: string;
  confirm_password: string;
  str: string;
}

export interface IRegisterWithAPIRequestBody {
  email: string;
  firstname: string;
  lastname: string;
  pseudo: string;
  address: string;
  num_address: string;
  postal_code: string;
  country: string;
  city: string;
  sexe: string;
  birthday: string;
  newsletter?: boolean;
}

export interface IRegisterWithGoogleRequestBody {
  email: string;
  googleId: string;
  firstname: string;
  lastname: string;
  pseudo: string;
  address: string;
  num_address: string;
  postal_code: string;
  country: string;
  city: string;
  sexe: string;
  birthday: string;
  newsletter?: boolean;
}

export interface IRegisterWithFacebookRequestBody {
  email: string;
  facebookId: string;
  firstname: string;
  lastname: string;
  pseudo: string;
  address: string;
  num_address: string;
  postal_code: string;
  country: string;
  city: string;
  sexe: string;
  birthday: string;
  newsletter?: boolean;
}

export interface IRegisterWithGithubRequestBody {
  email: string;
  githubId: string;
  firstname: string;
  lastname: string;
  pseudo: string;
  address: string;
  num_address: string;
  postal_code: string;
  country: string;
  city: string;
  sexe: string;
  birthday: string;
  newsletter?: boolean;
}

export interface ILoginWithGoogleRequestBody {
  email: string;
  googleId: string;
}

export interface ILoginWithFacebookRequestBody {
  email: string;
  facebookId: string;
}

export interface ILoginWithGithubRequestBody {
  email: string;
  githubId: string;
}
