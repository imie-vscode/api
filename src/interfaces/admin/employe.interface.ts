export interface IUpdateEmployeRequestBody {
    firstname: string;
    lastname: string;
    pseudo: string;
    address: string;
    num_address: string;
    postal_code: string;
    country: string;
    sexe: string;
    birthday: string | Date;
    role: String;
  }
  