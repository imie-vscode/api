export interface IAdminRegisterRequestBody {
  email: string;
  firstname: string;
  lastname: string;
  pseudo: string;
  password: string;
  confirm_password: string;
  address: string;
  num_address: string;
  postal_code: string;
  country: string;
  sexe: string;
  birthday: number;
  role: string;
}

export interface IadminLoginRequestBody {
  email: string;
  password: string;
}



