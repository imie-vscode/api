export interface ICreateSubscriptionRequestBody {
  name: string;
  description: string;
  price: number;
  advantages?: string[];
}
