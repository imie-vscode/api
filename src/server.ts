require("dotenv").config();
import express, { Application } from "express";
import helmet from "helmet";
import http from "http";
import cors from "cors";
import router from "./routes";
import { connectDb } from "./database";
import { connectGMAIL } from "./utils/email";
import morgan from "morgan";

// cache
declare global {
  var myCache: NodeCache;
}
import NodeCache from "node-cache";
import Socket from "./Socket";
import { errorLogger, logger } from "./config/winston";
import "./config/cronjob";
import { Request, Response } from "express";
import fs from "fs";
import path from "path";

global.myCache = new NodeCache();

const app: Application = express();
const httpServer = http.createServer(app);
const PORT = process.env.PORT || 5000;

app.use(require("express-status-monitor")());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(helmet());
app.use(morgan("combined", { stream: logger.stream }));
app.use(cors());
// app.use((req, res, next) => {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   res.header("Access-Control-Allow-Methods", "GET","POST");

//   next();
// });

// routes de l'api
app.get("/api", (req: Request, res: Response) => {
  res.sendStatus(200);
});

app.use(router);
export const connectServer = () => {
  return httpServer.listen(PORT);
};

(async () => {
  try {
    // on teste si le dossier /projects existe
    if (!fs.existsSync(path.resolve("projects"))) {
      fs.mkdirSync(path.resolve("projects"));
    }

    const server = connectServer();
    connectDb();
    connectGMAIL();
    Socket(server);
    console.log(`Serveur lancé sur le port ${PORT}`);
  } catch (error) {
    errorLogger.error(`${error.status || 500} - [src/server] - ${error.message}`);
    console.log("error: ", error);
  }
})();
