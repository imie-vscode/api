import { NextFunction, Request, Response } from "express";
import { isExisted, isEmail, isPassword, isBirthday, isNumeric, isPostalCode } from "../utils/validators";
import {
  IRegisterRequestBody,
  ILoginRequestBody,
  IForgotPasswordRequestBody,
  IResetPasswordRequestBody,
  IRegisterWithGoogleRequestBody,
} from "../interfaces/auth.interface";
import { SEXE } from "../utils/constantes";
import { IRegisterWithGithubRequestBody, IRegisterWithAPIRequestBody } from "../interfaces/auth.interface";

export const registerMiddleware = (req: Request, res: Response, next: NextFunction) => {
  try {
    const body = req.body as IRegisterRequestBody;
    const errors = [] as string[];

    // #############################
    // vérifications si les champs existent
    // #############################
    if (!isExisted(body.email)) errors.push("Champ courriel obligatoire");
    if (!isExisted(body.sexe)) errors.push("Champ sexe obligatoire");
    if (!isExisted(body.birthday)) errors.push("Champ date de naissance obligatoire");
    if (!isExisted(body.firstname)) errors.push("Champ prénom obligatoire");
    if (!isExisted(body.lastname)) errors.push("Champ nom obligatoire");
    if (!isExisted(body.pseudo)) errors.push("Champ pseudo obligatoire");
    if (!isExisted(body.password)) errors.push("Champ mot de passe obligatoire");
    if (!isExisted(body.confirm_password)) errors.push("Champ confirmation du mot de passe obligatoire");
    if (!isExisted(body.address)) errors.push("Champ adresse obligatoire");
    if (!isExisted(body.city)) errors.push("Champ ville obligatoire");
    if (!isExisted(body.num_address)) errors.push("Champ numéro de l'adresse obligatoire");
    if (!isExisted(body.postal_code)) errors.push("Champ code postal obligatoire");
    if (!isExisted(body.country)) errors.push("Champ pays obligatoire");

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    // #############################
    //   vérification des formats
    // #############################
    if (!isEmail(body.email)) errors.push("Le champ courriel n'est pas au bon format");
    if (!isPassword(body.password))
      errors.push(
        "Le champ mot de passe n'est pas au bon format (au moins 1 majuscule, 1 minuscule, 1 nombre, 1 caractère spécial, 8 caractères minimum)"
      );
    if (!isBirthday(body.birthday)) errors.push("Le champ date d'anniversaire n'est pas au bon format");
    if (!isNumeric(body.num_address)) errors.push("Le champ numéro adresse n'est pas au bon format");
    if (!isPostalCode(body.postal_code)) errors.push("Le champ code postal n'est pas au bon format");
    if (typeof body.address !== "string") errors.push("Le champ adresse n'est pas au bon format");
    if (typeof body.city !== "string") errors.push("Le champ ville n'est pas au bon format");
    if (typeof body.country !== "string") errors.push("Le champ pays n'est pas au bon format");
    if (typeof body.firstname !== "string") errors.push("Le champ prénom n'est pas au bon format");
    if (typeof body.lastname !== "string") errors.push("Le champ nom n'est pas au bon format");
    if (typeof body.pseudo !== "string") errors.push("Le champ pseudo n'est pas au bon format");
    if (!SEXE.includes(body.sexe)) errors.push("Le champ sexe n'est pas au bon format");

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    // #############################
    //     longueur des champs
    // #############################
    if (body.lastname.length > 50) errors.push("Le champ nom ne doit pas exéder 50 caractères");
    if (body.firstname.length > 50) errors.push("Le champ prénom ne doit pas exéder 50 caractères");
    if (body.country.length > 50) errors.push("Le champ nom ne doit pas exéder 50 caractères");
    if (body.pseudo.length > 50) errors.push("Le champ pseudo ne doit pas exéder 50 caractères");
    if (body.num_address.length > 50) errors.push("Le champ nom ne doit pas exéder 50 caractères");
    if (body.address.length > 255) errors.push("Le champ nom ne doit pas exéder 255 caractères");
    if (body.city.length > 255) errors.push("Le champ ville ne doit pas exéder 255 caractères");
    if (body.password.length > 255) errors.push("Le champ mot de passe ne doit pas exéder 255 caractères");
    if (body.email.length > 255) errors.push("Le champ courriel ne doit pas exéder 255 caractères");

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    // #############################
    // vérification si les mots de passes correspondent
    // #############################
    if (body.password !== body.confirm_password) {
      return res.status(409).json({ error: "Les mots de passe ne correspondent pas" });
    }

    next();
  } catch (error) {
    console.log("error: ", error);
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const loginMiddleware = (req: Request, res: Response, next: NextFunction) => {
  try {
    const body = req.body as ILoginRequestBody;
    const errors = [] as string[];
    // vérifications si les champs existent
    if (!isExisted(body.email)) errors.push("Champ courriel obligatoire");
    if (!isExisted(body.password)) errors.push("Champ mot de passe obligatoire");

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    next();
  } catch (error) {
    console.log("error: ", error);
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const forgotPasswordMiddleware = (req: Request, res: Response, next: NextFunction) => {
  try {
    const body = req.body as IForgotPasswordRequestBody;
    // vérifications si les champs existent
    if (!isExisted(body.email)) return res.status(409).json({ error: "Champ courriel obligatoire" });

    next();
  } catch (error) {
    console.log("error: ", error);
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const resetPasswordMiddleware = (req: Request, res: Response, next: NextFunction) => {
  try {
    const body = req.body as IResetPasswordRequestBody;
    const errors = [] as string[];

    // #############################
    // vérifications si les champs existent
    // #############################
    if (!isExisted(body.password)) return res.status(409).json({ error: "Champ mot de passe obligatoire" });
    if (!isExisted(body.confirm_password)) errors.push("Champ confirmation du mot de passe obligatoire");

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    // #############################
    //   vérification des formats
    // #############################
    if (!isPassword(body.password))
      errors.push(
        "Le champ mot de passe n'est pas au bon format (au moins 1 majuscule, 1 minuscule, 1 nombre, 1 caractère spécial, 8 caractères minimum)"
      );

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    // #############################
    // vérification si les mots de passes correspondent
    // #############################
    if (body.password !== body.confirm_password) {
      return res.status(409).json({ error: "Les mots de passe ne correspondent pas" });
    }

    next();
  } catch (error) {
    console.log("error: ", error);
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const registerWithAPIMiddleware = (req: Request, res: Response, next: NextFunction) => {
  try {
    const body = req.body as IRegisterWithAPIRequestBody;
    const errors = [] as string[];

    // #############################
    // vérifications si les champs existent
    // #############################
    if (!isExisted(body.email)) errors.push("Champ courriel obligatoire");
    if (!isExisted(body.sexe)) errors.push("Champ sexe obligatoire");
    if (!isExisted(body.birthday)) errors.push("Champ date de naissance obligatoire");
    if (!isExisted(body.firstname)) errors.push("Champ prénom obligatoire");
    if (!isExisted(body.lastname)) errors.push("Champ nom obligatoire");
    if (!isExisted(body.pseudo)) errors.push("Champ pseudo obligatoire");
    if (!isExisted(body.address)) errors.push("Champ adresse obligatoire");
    if (!isExisted(body.city)) errors.push("Champ ville obligatoire");
    if (!isExisted(body.num_address)) errors.push("Champ numéro de l'adresse obligatoire");
    if (!isExisted(body.postal_code)) errors.push("Champ code postal obligatoire");
    if (!isExisted(body.country)) errors.push("Champ pays obligatoire");

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    // #############################
    //   vérification des formats
    // #############################
    if (!isEmail(body.email)) errors.push("Le champ courriel n'est pas au bon format");
    if (!isBirthday(body.birthday)) errors.push("Le champ date d'anniversaire n'est pas au bon format");
    if (!isNumeric(body.num_address)) errors.push("Le champ numéro adresse n'est pas au bon format");
    if (!isPostalCode(body.postal_code)) errors.push("Le champ code postal n'est pas au bon format");
    if (typeof body.address !== "string") errors.push("Le champ adresse n'est pas au bon format");
    if (typeof body.city !== "string") errors.push("Le champ ville n'est pas au bon format");
    if (typeof body.country !== "string") errors.push("Le champ pays n'est pas au bon format");
    if (typeof body.firstname !== "string") errors.push("Le champ prénom n'est pas au bon format");
    if (typeof body.lastname !== "string") errors.push("Le champ nom n'est pas au bon format");
    if (typeof body.pseudo !== "string") errors.push("Le champ pseudo n'est pas au bon format");
    if (!SEXE.includes(body.sexe)) errors.push("Le champ sexe n'est pas au bon format");

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    // #############################
    //     longueur des champs
    // #############################
    if (body.lastname.length > 50) errors.push("Le champ nom ne doit pas exéder 50 caractères");
    if (body.firstname.length > 50) errors.push("Le champ prénom ne doit pas exéder 50 caractères");
    if (body.country.length > 50) errors.push("Le champ nom ne doit pas exéder 50 caractères");
    if (body.pseudo.length > 50) errors.push("Le champ pseudo ne doit pas exéder 50 caractères");
    if (body.num_address.length > 50) errors.push("Le champ nom ne doit pas exéder 50 caractères");
    if (body.address.length > 255) errors.push("Le champ nom ne doit pas exéder 255 caractères");
    if (body.city.length > 255) errors.push("Le champ ville ne doit pas exéder 255 caractères");
    if (body.email.length > 255) errors.push("Le champ courriel ne doit pas exéder 255 caractères");

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    next();
  } catch (error) {
    console.log("error: ", error);
    res.status(500).json({ error: "Erreur serveur" });
  }
};
