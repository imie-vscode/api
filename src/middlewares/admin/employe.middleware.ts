import { NextFunction, Request, Response } from "express";
import { IUpdateEmployeRequestBody } from "../../interfaces/admin/employe.interface";
import { PRIVATES_ROLES_ALLOWED } from "../../utils/constantes";
import { getToken } from "../../utils/token";
import { isExisted } from "../../utils/validators";




export const employeMiddleware = (req: Request, res: Response, next: NextFunction) => {
  if (!req.headers.authorization) {
    return res.status(401).json({ error: "Non autorisé" });
  }

  const bearer = req.headers.authorization as string;
  const token = getToken(bearer);

  if (!token) {
    return res.status(403).json({ error: "Non authentifié" });
  }
  
   if(!PRIVATES_ROLES_ALLOWED.includes(token.role)){
   return res.status(403).json({ error: "Vous n'avez pas les droits pour accéder à cette ressource" });}
  

  next();
};

export const updateEmployeMiddleware = (req: Request, res: Response, next: NextFunction) => {
  const body = req.body as IUpdateEmployeRequestBody;
  const errors = [] as string[];
  // vérifications si les champs existent
  if (!isExisted(body.sexe)) errors.push("Champ sexe obligatoire");
  if (!isExisted(body.birthday.toString())) errors.push("Champ date de naissance obligatoire");
  if (!isExisted(body.firstname)) errors.push("Champ prénom obligatoire");
  if (!isExisted(body.lastname)) errors.push("Champ nom obligatoire");
  if (!isExisted(body.pseudo)) errors.push("Champ pseudo obligatoire");
  if (!isExisted(body.address)) errors.push("Champ adresse obligatoire");
  if (!isExisted(body.num_address)) errors.push("Champ numéro de l'adresse obligatoire");
  if (!isExisted(body.postal_code)) errors.push("Champ code postal obligatoire");
  if (!isExisted(body.country)) errors.push("Champ pays obligatoire");

  // si y'a des erreurs, on retourne le tableau d'erreurs
  if (errors.length > 0) {
    return res.status(409).json({ error: errors });
  }

  //TODO: vérification des formats

  next();
};



