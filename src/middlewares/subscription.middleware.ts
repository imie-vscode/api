import { NextFunction, Request, Response } from "express";
import { isExisted, isAlphaNumeric, isPrice } from "../utils/validators";
import { ICreateSubscriptionRequestBody } from "../interfaces/subscription.interface";

export const createSubscriptionMiddleware = (req: Request, res: Response, next: NextFunction) => {
  try {
    const body = req.body as ICreateSubscriptionRequestBody;
    const errors = [] as string[];
    // vérifications si les champs existent
    if (!isExisted(body.name)) errors.push("Champ nom de l'abonnement obligatoire");
    if (!isExisted(body.description)) errors.push("Champ description de l'abonnement obligatoire");
    if (!isExisted(body.price)) errors.push("Champ prix de l'abonnement obligatoire");

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    // on vérifie les formats des données envoyés
    if (!isAlphaNumeric(body.name)) errors.push("Le champ nom de l'abonnement n'est pas au bon format");
    if (typeof body.description !== "string") errors.push("Le champ description de l'abonnement n'est pas au bon format");
    if (!isPrice(body.price)) errors.push("Le champ prix de l'abonnement n'est pas au bon format");

    // si on renseigne des avantages qui ne sont pas obligatoires
    if (body.advantages) {
      // on vérifie que ce sont bien des chaînes de caractères
      for (const advantage of body.advantages) {
        if (typeof advantage !== "string") {
          errors.push("Certains avantages de l'abonnement ne sont pas au bon format");
          break;
        }
      }
    }

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    // vérification des tailles
    if (body.name.length > 25) errors.push("Le champ nom de l'abonnement ne doit pas exéder 25 caractères");
    if (body.description.length > 500) errors.push("Le champ nom de l'abonnement ne doit pas exéder 500 caractères");

    // si on renseigne des avantages qui ne sont pas obligatoires
    if (body.advantages) {
      // on vérifie que ce sont bien des chaînes de caractères
      for (const advantage of body.advantages) {
        if (advantage.length > 255) {
          errors.push("Les champs avantages de l'abonnement ne doivent pas exéder 255 caractères");
          break;
        }
      }
    }

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    next();
  } catch (error) {
    console.log("error: ", error);
    res.status(500).json({ error: "Erreur serveur" });
  }
};
