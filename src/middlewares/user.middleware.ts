import { NextFunction, Request, Response } from "express";
import { getToken } from "../utils/token";
import { isBirthday, isExisted, isNumeric, isPostalCode, isCardNumber, isExp, isCVC } from "../utils/validators";
import {
  IUpdateUserRequestBody,
  IUpdateUserSubscriptionRequestBody,
  INewProjectRequestBody,
  IAddUserInProjectRequestBody,
} from "../interfaces/user.interface";
import { SEXE, TEMPLATES } from "../utils/constantes";

export const userMiddleware = (req: Request, res: Response, next: NextFunction) => {
  try {
    if (!req.headers.authorization) {
      return res.status(401).json({ error: "Non autorisé" });
    }

    const bearer = req.headers.authorization as string;
    const token = getToken(bearer);

    if (!token) {
      return res.status(403).json({ error: "Non authentifié" });
    }

    next();
  } catch (error) {
    console.log("error: ", error);
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const updateUserMiddleware = (req: Request, res: Response, next: NextFunction) => {
  try {
    const body = req.body as IUpdateUserRequestBody;
    const errors = [] as string[];

    // #############################
    // vérifications si les champs existent
    // #############################
    if (!isExisted(body.sexe)) errors.push("Champ sexe obligatoire");
    if (!isExisted(body.birthday)) errors.push("Champ date de naissance obligatoire");
    if (!isExisted(body.firstname)) errors.push("Champ prénom obligatoire");
    if (!isExisted(body.lastname)) errors.push("Champ nom obligatoire");
    if (!isExisted(body.pseudo)) errors.push("Champ pseudo obligatoire");
    if (!isExisted(body.address)) errors.push("Champ adresse obligatoire");
    if (!isExisted(body.city)) errors.push("Champ ville obligatoire");
    if (!isExisted(body.num_address)) errors.push("Champ numéro de l'adresse obligatoire");
    if (!isExisted(body.postal_code)) errors.push("Champ code postal obligatoire");
    if (!isExisted(body.country)) errors.push("Champ pays obligatoire");

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    // #############################
    //   vérification des formats
    // #############################
    if (!isBirthday(body.birthday)) errors.push("Le champ date d'anniversaire n'est pas au bon format");
    if (!isNumeric(body.num_address)) errors.push("Le champ numéro adresse n'est pas au bon format");
    if (!isPostalCode(body.postal_code)) errors.push("Le champ code postal n'est pas au bon format");
    if (typeof body.address !== "string") errors.push("Le champ adresse n'est pas au bon format");
    if (typeof body.city !== "string") errors.push("Le champ ville n'est pas au bon format");
    if (typeof body.country !== "string") errors.push("Le champ pays n'est pas au bon format");
    if (typeof body.firstname !== "string") errors.push("Le champ prénom n'est pas au bon format");
    if (typeof body.lastname !== "string") errors.push("Le champ nom n'est pas au bon format");
    if (typeof body.pseudo !== "string") errors.push("Le champ pseudo n'est pas au bon format");
    if (!SEXE.includes(body.sexe)) errors.push("Le champ sexe n'est pas au bon format");

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    // #############################
    //     longueur des champs
    // #############################
    if (body.lastname.length > 50) errors.push("Le champ nom ne doit pas exéder 50 caractères");
    if (body.firstname.length > 50) errors.push("Le champ prénom ne doit pas exéder 50 caractères");
    if (body.country.length > 50) errors.push("Le champ nom ne doit pas exéder 50 caractères");
    if (body.city.length > 255) errors.push("Le champ ville ne doit pas exéder 255 caractères");
    if (body.pseudo.length > 50) errors.push("Le champ pseudo ne doit pas exéder 50 caractères");
    if (body.num_address.length > 50) errors.push("Le champ nom ne doit pas exéder 50 caractères");
    if (body.address.length > 255) errors.push("Le champ nom ne doit pas exéder 255 caractères");

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    next();
  } catch (error) {
    console.log("error: ", error);
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const updateUserSubscriptionMiddleware = (req: Request, res: Response, next: NextFunction) => {
  try {
    const body = req.body as IUpdateUserSubscriptionRequestBody;

    const errors = [] as string[];

    // #############################
    // vérifications si les champs existent
    // #############################
    if (!isExisted(body.card_number)) errors.push("Champ numéro de carte bancaire obligatoire");
    if (!isExisted(body.exp)) errors.push("Champ expiration de la carte bancaire obligatoire");
    if (!isExisted(body.cvc)) errors.push("Champ CVC de la carte bancaire obligatoire");

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    // #############################
    //   vérification des formats
    // #############################
    if (!isCardNumber(body.card_number)) errors.push("Le champ numéro de carte bancaire n'est pas au bon format");
    if (!isExp(body.exp)) errors.push("Le champ expiration de la carte bancaire n'est pas au bon format");
    if (!isCVC(body.cvc)) errors.push("le champ CVC de la carte bancaire n'est pas au bon format");

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    // #############################
    //   si l'id n'est pas renseigné, on retourne une erreur afin d'éviter de faire une requête
    // #############################
    if (!isExisted(body.subscription_id)) {
      return res.status(404).json({ error: "Abonnement introuvable" });
    }

    next();
  } catch (error) {
    console.log("error: ", error);
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const newprojectMiddleware = (req: Request, res: Response, next: NextFunction) => {
  try {
    const body = req.body as INewProjectRequestBody;

    const errors = [] as string[];

    // #############################
    // vérifications si les champs existent
    // #############################
    if (!isExisted(body.name)) errors.push("Champ nom du projet obligatoire");

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    // #############################
    //   vérification des formats
    // #############################
    if (/[0-9]/.test(body.name)) errors.push("Le nom du projet ne doit pas comporter de chiffres");
    if (!TEMPLATES.includes(body.template)) errors.push("Template non reconnu");

    // si y'a des erreurs, on retourne le tableau d'erreurs
    if (errors.length > 0) {
      return res.status(409).json({ error: errors });
    }

    next();
  } catch (error) {
    console.log("error: ", error);
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const addUserInProjectMiddleware = (req: Request, res: Response, next: NextFunction) => {
  try {
    const body = req.body as IAddUserInProjectRequestBody;

    if (!isExisted(body.id_user)) {
      return res.status(400).json({ error: "Veuillez sélectionner un utilisateur" });
    }

    next();
  } catch (error) {
    console.log("error: ", error);
    res.status(500).json({ error: "Erreur serveur" });
  }
};
