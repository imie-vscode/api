import cron from "node-cron";
import { errorLogger } from "./winston";
import Zip from "node-zip";
import fs from "fs";
import moment from "moment";
import "moment/locale/fr";
import path from "path";
import { messageEmail, sendMail } from "../utils/email";
import { FAKE_EMAIL } from "../utils/constantes";
moment.locale("fr");

// tous les 1er du mois
cron.schedule("* * * 1 * *", () => {
  checkErrorLogs();
});

const checkErrorLogs = async () => {
  const zip = new Zip();
  try {
    const createdAt = new Date().getTime();
    const day = moment(createdAt).format("DD");
    const month = moment(createdAt).format("MM");
    const year = moment(createdAt).format("YYYY");

    const errorLogPath = path.resolve("logs/error.log");
    const appLogPath = path.resolve("logs/app.log");

    // on récupère le dossier des logs
    const errorFile = fs.readFileSync(errorLogPath, { encoding: "utf8" });

    // on récupère les requetes utilisateurs
    const requestFile = fs.readFileSync(appLogPath, { encoding: "utf8" });

    // on zip
    zip.file(`error.log`, errorFile);
    zip.file(`app.log`, requestFile);

    const dataZiped = zip.generate({ base64: false, compression: "DEFLATE" });

    // on stock le zip sur le serveur
    const filename = `${day}_${month}_${year}.zip`;
    const pathname = path.resolve(`logs/archives/${filename}`);
    if (!fs.existsSync(path.resolve(`logs/archives`))) {
      fs.mkdirSync(pathname, { recursive: true });
    }

    // si le zip existe on le supprime pour remettre un nouveau
    if (fs.existsSync(path.resolve(pathname))) {
      fs.rmSync(pathname, { recursive: true, force: true });
    }
    fs.writeFileSync(pathname, dataZiped, { flag: "w", encoding: "binary" });

    // on supprime les anciens fichiers de logs
    fs.rmSync(errorLogPath);
    fs.rmSync(appLogPath);

    // on recré les fichiers
    fs.writeFileSync(errorLogPath, "", { flag: "w" });
    fs.writeFileSync(appLogPath, "", { flag: "w" });

    // on envoie le mail
    const attachments = [
      {
        filename,
        content: dataZiped,
        encoding: "binary",
      },
    ];

    const message = messageEmail(FAKE_EMAIL, `Logs du ${day}/${month}/${year}`, `Logs du ${day}/${month}/${year}`, "", attachments);
    const isSent = await sendMail(message);
    if (!isSent) {
      throw "Erreur lors de l'envoi du mail";
    }
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(`500 - [config/cronjob => checkErrorLogs] - Erreur lors l'envoi du mail`);
  }
};
