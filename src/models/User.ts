import mongoose from "mongoose";
// Schéma
//TODO: ajouter le champ phoneNumber avec la logique et les vérifs
const User = new mongoose.Schema(
  {
    email: {
      type: String,
      match: [/\S+@\S+\.\S+/, "Courriel invalide"],
      required: [true, "Courriel manquant"],
      createIndexes: { unique: true },
      max: 255,
    },
    facebookId: {
      type: String,
      default: null,
    },
    googleId: {
      type: String,
      default: null,
    },
    githubId: {
      type: String,
      default: null,
    },
    sexe: {
      type: String,
      required: [true, "Sexe manquant"],
      default: "Homme",
    },
    birthday: {
      type: Date,
      required: [true, "Date de naissance manquante"],
      default: new Date(),
    },
    role: {
      type: String,
      default: "CLIENT",
    },
    firstname: {
      type: String,
      required: [true, "Prénom manquant"],
      max: 50,
    },
    lastname: {
      type: String,
      required: [true, "Nom manquant"],
      max: 50,
    },
    pseudo: {
      type: String,
      required: [true, "Pseudo manquant"],
      max: 50,
    },
    password: {
      type: String,
      //match: [/\S+@\S+\.\S+/, "courriel invalide"],
      required: [true, "Mot de passe manquant"],
      max: 255,
    },
    address: {
      type: String,
      required: [true, "Adresse manquante"],
      max: 255,
    },
    city: {
      type: String,
      required: [true, "Ville manquante"],
      max: 255,
    },
    numAddress: {
      type: String,
      required: [true, "Numéro de l'adresse manquante"],
      max: 50,
    },
    postalCode: {
      type: String,
      required: [true, "Code postal manquant"],
    },
    country: {
      type: String,
      required: [true, "Pays manquant"],
      max: 50,
    },
    active: {
      type: Boolean,
      default: true,
    },
    newsletter: {
      type: Boolean,
      default: false,
    },
    subscription: {
      name: {
        type: String,
        max: 25,
        default: "Gratuit",
      },
      description: {
        type: String,
        max: 500,
        default: "",
      },
      price: {
        type: String,
        default: "0,00",
      },
      advantages: [
        {
          type: String,
          max: 255,
        },
      ],
      subscribedSince: {
        type: String,
        default: new Date(),
      },
    },
    idStripe: {
      type: String,
      default: null,
    },
  },
  {
    timestamps: true,
  }
);

// Modèle
export default mongoose.model("users", User);
