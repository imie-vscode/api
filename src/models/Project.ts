import mongoose from "mongoose";
// Schéma
const Project = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Nom du projet manquant manquant"],
    },
    template: {
      type: String,
      required: [true, "Template du projet manquant manquant"],
    },
    path: {
      type: String,
    },
    entry: {
      type: String,
    },
    idUser: {
      type: mongoose.Types.ObjectId,
      required: [true, "ID client manquant"],
      ref: "users",
    },
    group: [
      {
        type: mongoose.Types.ObjectId,
        ref: "users",
      },
    ],
  },
  {
    timestamps: true,
  }
);

// Modèle
export default mongoose.model("projects", Project);
