import mongoose from "mongoose";
// Schéma
const Bill = new mongoose.Schema(
  {
    idStripe: {
      type: String,
      required: [true, "ID Stripe manquant"],
      createIndexes: { unique: true },
    },
    idSubscription: {
      type: mongoose.Types.ObjectId,
      required: [true, "Abonnement manquant"],
      ref: "subscriptions",
    },
    idUser: {
      type: mongoose.Types.ObjectId,
      required: [true, "ID client manquant"],
      ref: "users",
    },
  },
  {
    timestamps: true,
  }
);

// Modèle
export default mongoose.model("bills", Bill);
