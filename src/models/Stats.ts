import mongoose from "mongoose";
// Schéma
const Stats = new mongoose.Schema(
  {
    chars: {
      type: Number,
      default: 0,
    },
    files: {
      type: Number,
      default: 0,
    },
    folders: {
      type: Number,
      default: 0,
    },
    streamSessions: [
      {
        type: mongoose.Types.ObjectId,
        ref: "sessions",
      },
    ],
    idUser: {
      type: mongoose.Types.ObjectId,
      required: [true, "ID client manquant"],
      ref: "users",
    },
    idProject: {
      type: mongoose.Types.ObjectId,
      required: [true, "ID projet manquant"],
      ref: "projects",
    },
  },
  {
    timestamps: true,
  }
);

// Modèle
export default mongoose.model("stats", Stats);
