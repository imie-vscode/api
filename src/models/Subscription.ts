import mongoose from "mongoose";
// Schéma
const Subscription = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Nom de l'abonnement manquant"],
      createIndexes: { unique: true },
      max: 25,
    },
    description: {
      type: String,
      required: [true, "Description de l'abonnement manquante"],
      max: 500,
    },
    price: {
      type: String,
      //   match: [/^\d+([.,]\d{1,2})?$/, "prix invalide"],
      required: [true, "Prix de l'abonnement manquant"],
    },
    advantages: [
      {
        type: String,
        max: 255,
      },
    ],
  },
  {
    timestamps: true,
  }
);

// Modèle
export default mongoose.model("subscriptions", Subscription);
