import mongoose from "mongoose";
// Schéma
const Session = new mongoose.Schema(
  {
    uuid: {
      type: String,
      required: [true, "ID session manquant"],
      createIndexes: { unique: true },
    },
    idProject: {
      type: mongoose.Types.ObjectId,
      ref: "projects",
    },
    master: {
      type: mongoose.Types.ObjectId,
      ref: "users",
    },
    others: [
      {
        type: mongoose.Types.ObjectId,
        ref: "users",
      },
    ],
    time: {
      type: Number,
      default: null,
    },
    online: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
  }
);

// Modèle
export default mongoose.model("sessions", Session);
