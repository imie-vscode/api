require("dotenv").config();
import http from "http";
import Project from "./models/Project";
import dirTree from "directory-tree";
import { getToken, IToken } from "./utils/token";
import fs from "fs";
import path from "path";
import Session from "./models/Session";
import { v4 as uuidv4 } from "uuid";
import User from "./models/User";
import Stats from "./models/Stats";
import * as socketio from "socket.io";
import _path from "path";

interface IJoinRoom {
  id_project: string;
  id_user: string;
}

interface IJoinSession extends IJoinRoom {
  id_session: string;
}

interface IGetNumberParticipants {
  id_project: string;
}

interface IGetNumberSessionParticipants {
  id_session: string;
}

interface ILeaveRoom extends IJoinRoom {}

interface ICreateSession extends IJoinRoom {}

interface IGetUserInRoom extends IJoinRoom {}
interface IGetUserInSession extends IJoinRoom {
  id_session: string;
}

interface ILeaveSession extends IJoinRoom {}

interface IGetFileContent extends IJoinRoom {
  filename: string;
}
interface ISendCode extends IJoinRoom {
  filename: string;
  code: string;
  origin: "+delete" | "+input";
}

interface ICreateFolder extends IJoinRoom {
  path: string;
  root: string;
}

interface IRenameFolder extends IJoinRoom, ICreateFolder {
  oldFolder: string;
}

interface IDeleteFolder extends IJoinRoom {
  path: string;
  root: string;
}

interface ICreateFile extends IJoinRoom {
  path: string;
  root: string;
  filename: string;
  extension: string;
}

interface IRenameFile extends IJoinRoom, ICreateFile {
  oldFilename: string;
}

interface IDeleteFile extends IJoinRoom {
  path: string;
  root: string;
}

interface IStreamData extends IJoinRoom {
  image: string;
  id_session: string;
}

export default (server: http.Server) => {
  const io = new socketio.Server();

  io.listen(5001, {
    transports: ["websocket"],
    cors: {
      origin: process.env.SITE_URL,
      methods: ["GET"],
      credentials: true,
    },
  });
  console.log("SocketIO connecté");

  // on vérifie le token
  //   io.use((ws, next) => {
  //     const token = ws.handshake.query.token as IToken | undefined;
  //     const isAuthorized = tokenControllerSocket(token);
  //     if (!isAuthorized) {
  //       const err: any = new Error("Non autorisé");
  //       err.data = { code: 403 };
  //       next(err);
  //     } else {
  //       next();
  //     }
  //   });

  // on déclare une variable pour stocker les id utilisateurs
  // { id_user : id_project }
  let usersRooms = {} as { [key: string]: string };
  // { id_user : id_session }
  let sessions = {} as { [key: string]: string };
  io.on("connection", socket => {
    socket.on("JOIN_ROOM", async (data: IJoinRoom, response: any) => {
      try {
        // on ajoute l'utilisateur au salon
        socket.join(data.id_project);

        // on ajoute l'id de l'utilisateur en valeurs
        usersRooms[data.id_user] = data.id_project;

        // on envoie à l'utilisateur une réponse pour dire qu'il a rejoint la session
        response("JOINED");

        // ##########################################################
        //                  On récupère le projet
        // ##########################################################
        const project: any = await Project.findById(data.id_project)
          .populate({
            path: "idUser",
            model: "users",
          })
          .populate({
            path: "group",
            model: "users",
          });

        const users = {};

        if (project) {
          // on ajoute le maitre du projet
          users[project.idUser._id] = {
            _id: project.idUser._id,
            pseudo: project.idUser.pseudo,
            email: project.idUser.email,
            master: true,
            online: Object.keys(usersRooms).find(usr => usr === String(project.idUser._id)) ? true : false,
          };

          for (const gr of project.group) {
            // on vérifie si l'utilisateur est en ligne
            users[gr._id] = {
              _id: gr._id,
              pseudo: gr.pseudo,
              email: gr.email,
              master: false,
              online: Object.keys(usersRooms).find(usr => usr === String(gr._id)) ? true : false,
            };
          }
        }

        // on envoie le nombre de participants en cours dans le salon
        io.to(data.id_project).emit("ROOM_PARTICIPANTS", { users: Object.keys(users).length > 0 ? users : null });
      } catch (error) {
        console.log("error: ", error);
        return;
      }
    });

    socket.on("JOIN_SESSION", async (data: IJoinSession, response: any) => {
      try {
        // on ajoute l'utilisateur au salon
        socket.join(data.id_session);
        // on récupère les participants du salon
        const rooms = io.sockets.adapter.rooms;

        // on ajoute l'id de l'utilisateur en valeurs
        sessions[data.id_user] = data.id_session;

        // on calcul le nombre de participants dans le salon
        const nbUsers = rooms.get(data.id_session)?.size || 0;

        // on récupère la session en BDD
        const session: any = await Session.findOne({ uuid: data.id_session, idProject: data.id_project, online: true });

        if (!session) {
          return response({ code: 404, message: "Session introuvable ou hors ligne" });
        }

        // on vérifie si l'utilisateur n'existe pas dans le salon
        let exist = false;
        if (String(session.master) === data.id_user || session.others.find(other => String(other) === data.id_user)) {
          exist = true;
        }

        // si l'utilisateur n'existe pas dans la session, on l'ajoute
        if (!exist) {
          session.others = [...session.others, data.id_user];
          await session.save();
        }

        // on envoie à l'utilisateur une réponse pour dire qu'il a rejoint la session
        response({ code: 200, message: "Vous avez rejoint la session" });

        // on envoie le nombre de participants en cours dans le salon
        io.to(data.id_project).emit("NUMBER_SESSION_PARTICIPANTS", { users: nbUsers, id_session: data.id_session });
      } catch (error) {
        console.log("error: ", error);
        return;
      }
    });

    socket.on("GET_NUMBER_PARTICIPANTS", (data: IGetNumberParticipants, response: any) => {
      try {
        // on récupère les participants du salon
        const rooms = io.sockets.adapter.rooms;
        // on calcul le nombre de participants dans le salon
        const nbUsers = rooms.get(data.id_project)?.size || 0;
        // on envoie le nombre de participants en cours dans le salon
        response(nbUsers);
      } catch (error) {
        console.log("error: ", error);
        return;
      }
    });

    socket.on("GET_NUMBER_SESSION_PARTICIPANTS", (data: IGetNumberSessionParticipants, response: any) => {
      try {
        // on récupère les participants du salon
        const rooms = io.sockets.adapter.rooms;
        // on calcul le nombre de participants dans le salon
        const nbUsers = rooms.get(data.id_session)?.size || 0;
        response(nbUsers);
      } catch (error) {
        console.log("error: ", error);
        return;
      }
    });

    socket.on("LEAVE_ROOM", async (data: ILeaveRoom) => {
      try {
        // on fait quitter le salon à l'utilisateur
        socket.leave(data.id_project);

        // on supprime l'id de l'utilisateur en valeurs
        delete usersRooms[data.id_user];

        // ##########################################################
        //                  On récupère le projet
        // ##########################################################
        const project: any = await Project.findById(data.id_project)
          .populate({
            path: "idUser",
            model: "users",
          })
          .populate({
            path: "group",
            model: "users",
          });

        const users = {};

        if (project) {
          // on ajoute le maitre du projet
          users[project.idUser._id] = {
            _id: project.idUser._id,
            pseudo: project.idUser.pseudo,
            email: project.idUser.email,
            master: true,
            online: Object.keys(usersRooms).find(usr => usr === String(project.idUser._id)) ? true : false,
          };

          for (const gr of project.group) {
            // on vérifie si l'utilisateur est en ligne
            users[gr._id] = {
              _id: gr._id,
              pseudo: gr.pseudo,
              email: gr.email,
              master: false,
              online: Object.keys(usersRooms).find(usr => usr === String(gr._id)) ? true : false,
            };
          }
        }

        // on envoie le nombre de participants en cours dans le salon
        io.to(data.id_project).emit("ROOM_PARTICIPANTS", { users: Object.keys(users).length > 0 ? users : null });
      } catch (error) {
        console.log("error: ", error);
        return;
      }
    });

    socket.on("LIST_ONLINE_SESSION_IN_THE_PROJECT", async (data: IJoinSession, response: any) => {
      try {
        // ##########################################################
        //                  Récupération des sessions
        // ##########################################################
        const session: any = await Session.findOne({ idProject: data.id_project, online: true })
          .populate({
            path: "master",
            model: "users",
          })
          .populate({
            path: "others",
            model: "users",
          });

        if (!session) {
          return;
        }

        // on map les participants
        const others = [];

        for (const other of session.others) {
          others.push({
            _id: other._id,
            email: other.email,
            pseudo: other.pseudo,
            firstname: other.firstname,
            lastname: other.lastname,
          });
        }

        // on formatte les données
        const dataSession = {
          _id: session._id,
          uuid: session.uuid,
          master: {
            _id: session.master._id,
            email: session.master.email,
            pseudo: session.master.pseudo,
            firstname: session.master.firstname,
            lastname: session.master.lastname,
          },
          others,
          online: session.online,
          time: session.time,
          createdAt: session.createdAt,
        };

        response({ code: 200, session: dataSession });
      } catch (error) {
        console.log("error: ", error);
        return;
      }
    });

    socket.on("CREATE_SESSION", async (data: ICreateSession, response: any) => {
      try {
        // ##########################################################
        //            On vérifie si une session existe déjà
        // ##########################################################
        const sessionExist: any = await Session.findOne({ idProject: data.id_project, online: true }, { master: 0, others: 0 });

        if (sessionExist) {
          return response({ code: 400, message: "Une session pour ce projet est déjà en ligne" });
        }

        // ##########################################################
        //              Vérification si l'utilisateur existe
        // ##########################################################
        const user: any = await User.findById(data.id_user, {
          password: 0,
          address: 0,
          numAddress: 0,
          city: 0,
          postalCode: 0,
          idStripe: 0,
          phoneNumber: 0,
        });
        if (!user) {
          return response({ code: 404, message: "Utilisateur introuvable" });
        }

        // si l'utilisateur a un abonnement gratuit, on empêche la création d'une session
        if (/^gratuit$/i.test(user.subscription.name)) {
          return response({
            code: 400,
            message: "Votre abonnement ne permet pas la création de sessions. Veuillez le mettre à jour pour bénéficier de ce service",
          });
        }

        // ##########################################################
        //              Vérification si le projet existe
        // ##########################################################
        const project: any = await Project.findOne(
          { _id: data.id_project, $or: [{ idUser: data.id_user }, { group: data.id_user }] },
          { __v: 0, idUser: 0, group: 0 }
        );
        if (!project) {
          return response({ code: 404, message: "Projet introuvable" });
        }

        // ##########################################################
        //                  Création de la session
        // ##########################################################
        let str = "";
        let exist = false;
        do {
          str = uuidv4();
          // on vérifie si l'uuid n'existe pas
          const session = await Session.findOne({ uuid: str }, { idProject: 0, master: 0, others: 0 });
          exist = session ? true : false;
        } while (exist);

        // nouvelle session
        const session: any = await new Session({
          uuid: str,
          idProject: project,
          master: data.id_user,
          online: true,
        }).save();

        // on formatte les données
        const dataSession = {
          _id: session._id,
          uuid: session.uuid,
          master: {
            _id: user._id,
            email: user.email,
            pseudo: user.pseudo,
            firstname: user.firstname,
            lastname: user.lastname,
          },
          others: [],
          online: session.online,
          time: session.time,
          createdAt: session.createdAt,
        };

        // on me tà jour les stats utilisateurs
        miseAJourDesSessions(data, session._id);

        // on envoie la sassion créée dans le salon du projet
        io.to(data.id_project).emit("NEW_SESSION_CREATED", { session: dataSession });
        response({ code: 201, message: "Nouvelle session créé !" });
      } catch (error) {
        console.log("error: ", error);
        return;
      }
    });

    socket.on("GET_USERS_IN_ROOM", async (data: IGetUserInRoom, response: any) => {
      try {
        // ##########################################################
        //                  On récupère le projet
        // ##########################################################
        const project: any = await Project.findById(data.id_project)
          .populate({
            path: "idUser",
            model: "users",
          })
          .populate({
            path: "group",
            model: "users",
          });

        if (!project) {
          return response({ code: 400, message: "Projet introuvable" });
        }

        const users = {};
        // on ajoute le maitre du projet
        users[project.idUser._id] = {
          _id: project.idUser._id,
          pseudo: project.idUser.pseudo,
          email: project.idUser.email,
          master: true,
          online: Object.keys(usersRooms).find(usr => usr === String(project.idUser._id)) ? true : false,
        };

        for (const gr of project.group) {
          // on vérifie si l'utilisateur est en ligne
          users[gr._id] = {
            _id: gr._id,
            pseudo: gr.pseudo,
            email: gr.email,
            master: false,
            online: Object.keys(usersRooms).find(usr => usr === String(gr._id)) ? true : false,
          };
        }

        response({ code: 200, users: Object.keys(users).length > 0 ? users : null });
      } catch (error) {
        console.log("error: ", error);
        return;
      }
    });

    socket.on("GET_USERS_IN_SESSION", async (data: IGetUserInSession, response: any) => {
      try {
        // ##########################################################
        //                  On récupère la session
        // ##########################################################
        const session: any = await Session.findOne({ idProject: data.id_project, online: true })
          .populate({
            path: "master",
            model: "users",
          })
          .populate({
            path: "others",
            model: "users",
          });

        if (!session) {
          return response({ code: 400, message: "Session introuvable ou hors ligne" });
        }

        const users = {};
        // on ajoute le maitre de la session s'il est en ligne
        if (Object.keys(sessions).find(_id => String(_id) === String(session.master._id))) {
          users[session.master._id] = {
            _id: session.master._id,
            pseudo: session.master.pseudo,
            email: session.master.email,
            master: true,
          };
        }
        for (const other of session.others) {
          users[other._id] = {
            _id: other._id,
            pseudo: other.pseudo,
            email: other.email,
            master: false,
          };
        }

        response({ code: 200, users: Object.keys(users).length > 0 ? users : null });
      } catch (error) {
        console.log("error: ", error);
        return;
      }
    });

    socket.on("LEAVE_SESSION", async (data: ILeaveSession) => {
      try {
        const id_session = sessions[data.id_user];

        // on fait quitter le salon à l'utilisateur
        socket.leave(id_session);

        // on récupère les participants du salon
        const rooms = io.sockets.adapter.rooms;

        // on calcul le nombre de participants dans le salon
        const nbUsers = rooms.get(id_session)?.size || 0;

        // on récupère la session en BDD
        const session: any = await Session.findOne({ uuid: id_session, idProject: data.id_project, online: true });

        // on ne retourne pas d'erreur pour ne pas bloquer un utilisateur qui quitte la session
        if (session) {
          // si c'est le maître de la session qui quitte, on ferme la session en enlevant les utilisateurs et en faisant les calculs de durée
          if (String(session.master) === data.id_user) {
            // on envoie un message pour faire quitter tous les utilisateurs dans le salon du projet qui serait connectés à la session
            io.to(data.id_project).emit("SESSION_CLOSED", { id_session, message: "La session s'est terminée" });

            // on supprime tous les utilisateurs de l'objet session (le maître est supprimé au dessous dans cette condition)
            for (const other of session.others) {
              delete sessions[other];
            }
            const time = Date.now() - new Date(session.createdAt).getTime();
            session.others = [];
            session.time = time;
            session.online = false;
            await session.save();
          } else {
            const others = JSON.parse(JSON.stringify(session.others));
            for (let i = 0; i < session.others.length; i++) {
              const element = session.others[i];
              if (String(element) === data.id_user) {
                others.splice(i, 1);
                break;
              }
            }
            // on met à jour le salon
            session.others = others;
            await session.save();

            // on envoie le nombre de participants en cours dans le salon
            socket.broadcast.in(id_session).emit("NUMBER_SESSION_PARTICIPANTS", { users: nbUsers, id_session });
          }
        }
      } catch (error) {
        console.log("error: ", error);
      } finally {
        // on supprime l'id de l'utilisateur en valeurs
        delete sessions[data.id_user];
      }
    });

    socket.on("GET_FILE_CONTENT", (data: IGetFileContent, response: any) => {
      const filename = data.filename;
      try {
        // on véirfie si le fichier existe
        if (!fs.existsSync(filename)) {
          response({ code: 404, message: "Fichier introuvable" });
          return;
        }
        // on récupère le contenu du fichier
        const content = fs.readFileSync(filename, { encoding: "utf8" });
        const ext = path.extname(data.filename).replace(".", "");

        // on envoie le contenu du fichier et son extension
        response({ content, ext, code: 200, message: "OK" });
      } catch (error) {
        console.log("error: ", error);
        return;
      }
    });

    socket.on("CREATE_FOLDER", (data: ICreateFolder, response: any) => {
      // on vérifie si le dossier existe
      let exist = true;
      let path = `${data.path}`;
      let number = 0;
      try {
        do {
          if (fs.existsSync(path)) {
            const newPath = path.split("(");
            if (newPath.length > 1) {
              number = Number(newPath[1].split(")")[0]);
              newPath.pop();
            }
            path = `${newPath[0]}(${number + 1})`;
          } else {
            // on crée le dossier
            fs.mkdirSync(path);
            exist = false;
          }
        } while (exist);

        // on récupère la nouvelle structure du projet
        const tree = dirTree(`${data.root}`, { exclude: /node_modules/ });
        io.to(data.id_project).emit("UPDATE_PROJECT_STRUCTURE", { tree: tree?.children });
        response({ code: 201, message: "Dossier créé !" });
      } catch (error) {
        console.log("error: ", error);
        response({ code: 500, message: "Erreur lors de la création du dossier" });
      } finally {
        miseAJourDesStatsDesDossiersCrees(data, "CREATE");
      }
    });

    socket.on("RENAME_FOLDER", (data: IRenameFolder, response: any) => {
      // si l'ancien dossier porte le même nom que le nouveau, on ne fait rien
      if (data.oldFolder === data.path) {
        return;
      }
      // on vérifie si le dossier existe
      let exist = true;
      let path = `${data.path}`;
      let number = 0;

      try {
        do {
          if (fs.existsSync(`${path}`)) {
            const newPath = path.split("(");
            if (newPath.length > 1) {
              number = Number(newPath[1].split(")")[0]);
              newPath.pop();
            }
            path = `${newPath[0]}(${number + 1})`;
          } else {
            // on crée le dossier
            fs.renameSync(`${data.oldFolder}`, path);

            exist = false;
          }
        } while (exist);

        // on récupère la nouvelle structure du projet
        const tree = dirTree(`${data.root}`, { exclude: /node_modules/ });
        io.to(data.id_project).emit("UPDATE_PROJECT_STRUCTURE", { tree: tree?.children });
        response({ code: 201, message: "Dossier renommé !" });
      } catch (error) {
        console.log("error: ", error);
        response({ code: 500, message: "Erreur lors de l'étape de renommage du dossier" });
      }
    });

    socket.on("DELETE_FOLDER", (data: IDeleteFolder, response: any) => {
      const path = `${data.path}`;
      try {
        // si le fichier n'existe pas, on ne fait rien
        if (!fs.existsSync(`${path}`)) {
          return;
        }
        fs.rmSync(path, { recursive: true, force: true });
        // on récupère la nouvelle structure du projet
        const tree = dirTree(`${data.root}`, { exclude: /node_modules/ });
        io.to(data.id_project).emit("UPDATE_PROJECT_STRUCTURE", { tree: tree?.children });
        response({ code: 201, message: "Dossier supprimé !" });
      } catch (error) {
        console.log("error: ", error);
        response({ code: 500, message: "Erreur lors de la suppression du dossier" });
      } finally {
        miseAJourDesStatsDesDossiersCrees(data, "CREATE");
      }
    });

    socket.on("CREATE_FILE", (data: ICreateFile, response: any) => {
      // on vérifie si le dossier existe
      let exist = true;
      let path = `${data.path}/${data.filename}${data.extension}`;
      let number = 0;
      try {
        do {
          if (fs.existsSync(path)) {
            const newPathWithoutExtension = path.split(data.extension)[0];
            const newPath = newPathWithoutExtension.split("(");
            if (newPath.length > 1) {
              number = Number(newPath[1].split(")")[0]);
              newPath.pop();
            }
            path = `${newPath[0]}(${number + 1})${data.extension}`;
          } else {
            // on crée le dossier
            fs.appendFileSync(path, "");
            exist = false;
          }
        } while (exist);

        // on récupère la nouvelle structure du projet
        const tree = dirTree(`${data.root}`, { exclude: /node_modules/ });
        io.to(data.id_project).emit("UPDATE_PROJECT_STRUCTURE", { tree: tree?.children });
        response({ code: 201, message: "Fichier créé !" });
      } catch (error) {
        console.log("error: ", error);
        response({ code: 500, message: "Erreur lors de la création du fichier" });
      } finally {
        miseAJourDesStatsDesFichiersCrees(data, "DELETE");
      }
    });

    socket.on("RENAME_FILE", (data: IRenameFile, response: any) => {
      let path = `${data.path}/${data.filename}${data.extension}`;
      // si l'ancien fichier porte le même nom que le nouveau, on ne fait rien
      if (data.oldFilename === path) {
        return;
      }

      let exist = true;
      let number = 0;

      try {
        do {
          if (fs.existsSync(path)) {
            const newPathWithoutExtension = path.split(data.extension)[0];
            const newPath = newPathWithoutExtension.split("(");
            if (newPath.length > 1) {
              number = Number(newPath[1].split(")")[0]);
              newPath.pop();
            }
            path = `${newPath[0]}(${number + 1})${data.extension}`;
          } else {
            // on renome le fichier
            fs.renameSync(`${data.oldFilename}`, path);
            exist = false;
          }
        } while (exist);

        // on récupère la nouvelle structure du projet
        const tree = dirTree(`${data.root}`, { exclude: /node_modules/ });
        io.to(data.id_project).emit("UPDATE_PROJECT_STRUCTURE", { tree: tree?.children });
        response({ code: 201, message: "Fichier renommé !" });
      } catch (error) {
        console.log("error: ", error);
        response({ code: 500, message: "Erreur lors de l'étape de renommage du fichier" });
      }
    });

    socket.on("DELETE_FILE", (data: IDeleteFile, response: any) => {
      const path = `${data.path}`;
      try {
        // si le fichier n'existe pas, on ne fait rien
        if (!fs.existsSync(path)) {
          return;
        }
        fs.rmSync(path, { force: true });
        // on récupère la nouvelle structure du projet
        const tree = dirTree(`${data.root}`, { exclude: /node_modules/ });
        io.to(data.id_project).emit("UPDATE_PROJECT_STRUCTURE", { tree: tree?.children });
        response({ code: 201, message: "Fichier supprimé !" });
      } catch (error) {
        console.log("error: ", error);
        response({ code: 500, message: "Erreur lors de la suppression du fichier" });
      } finally {
        miseAJourDesStatsDesFichiersCrees(data, "DELETE");
      }
    });

    socket.on("SEND_CODE", (data: ISendCode, response: any) => {
      const filename = data.filename;
      try {
        // on écrit le contenu reçu dans le fichier
        const ws = fs.createWriteStream(path.resolve(filename), { flags: "w" });
        ws.once("open", function (fd) {
          ws.write(data.code);
          ws.end();
        });
        ws.on("finish", () => {
          // on vérifie si l'utilisateur est connecté à un salon
          // s'il n'est pas connecté on ne fait rien
          // s'il est connecté, on envoie le code à tous les participants du salon
          socket.broadcast.in(data.id_project).emit("RECEIVE_CODE", { code: data.code, filename: data.filename });
        });
        ws.on("error", err => {
          console.log("erreur", err);
          response({ code: 500, message: "Erreur serveur, veuillez réessayer" });
        });
      } catch (error) {
        console.log("error: ", error);
        return;
      } finally {
        miseAJourDesStatsDuCode(data);
      }
    });

    socket.on("STREAM_DATA", (data: IStreamData) => {
      try {
        socket.broadcast.in(data.id_session).emit("BROADCAST_STREAM_DATA", data.image);
      } catch (error) {
        console.log("error: ", error);
        return;
      }
    });
  });
};

const tokenControllerSocket = (token: IToken) => {
  return getToken(`Bearer ${token}`);
};

const miseAJourDesStatsDuCode = async (data: ISendCode) => {
  try {
    // ##########################################################
    //        On récupère le modèle des stats s'il existe
    // ##########################################################
    const todayStart = new Date();
    const todayEnd = new Date();
    let stats: any | null = await Stats.findOne({
      idUser: data.id_user,
      idProject: data.id_project,
      createdAt: {
        $gte: new Date(todayStart.getFullYear(), todayStart.getMonth(), todayStart.getDate(), 0, 0, 0, 0),
        $lt: new Date(todayEnd.getFullYear(), todayEnd.getMonth(), todayEnd.getDate(), 23, 59, 59, 999),
      },
    });

    // si un modèle existe
    if (stats) {
      // on le met à jour en l'incrémentant ou le décrément si l'on supprime un caractère
      stats.chars = data.origin === "+input" ? stats.chars + 1 : stats.chars - 1;
    } else {
      // sinon on crée le modèle pour le jour
      stats = new Stats({
        idUser: data.id_user,
        idProject: data.id_project,
        chars: data.origin === "+input" ? 1 : -1,
      });
    }

    // on met à jour les données
    await stats.save();
  } catch (error) {
    console.log("error: ", error);
    return;
  }
};

const miseAJourDesStatsDesFichiersCrees = async (data: IJoinRoom, action: "CREATE" | "DELETE") => {
  try {
    // ##########################################################
    //        On récupère le modèle des stats s'il existe
    // ##########################################################
    const todayStart = new Date();
    const todayEnd = new Date();
    let stats: any | null = await Stats.findOne({
      idUser: data.id_user,
      idProject: data.id_project,
      createdAt: {
        $gte: new Date(todayStart.getFullYear(), todayStart.getMonth(), todayStart.getDate(), 0, 0, 0, 0),
        $lt: new Date(todayEnd.getFullYear(), todayEnd.getMonth(), todayEnd.getDate(), 23, 59, 59, 999),
      },
    });

    // si un modèle existe
    if (stats) {
      // on le met à jour en l'incrémentant
      stats.files = action === "CREATE" ? stats.files + 1 : stats.files - 1;
    } else {
      // sinon on crée le modèle pour le jour
      stats = new Stats({
        idUser: data.id_user,
        idProject: data.id_project,
        files: action === "CREATE" ? 1 : -1,
      });
    }

    // on met à jour les données
    await stats.save();
  } catch (error) {
    console.log("error: ", error);
    return;
  }
};

const miseAJourDesStatsDesDossiersCrees = async (data: IJoinRoom, action: "CREATE" | "DELETE") => {
  try {
    // ##########################################################
    //        On récupère le modèle des stats s'il existe
    // ##########################################################
    const todayStart = new Date();
    const todayEnd = new Date();
    let stats: any | null = await Stats.findOne({
      idUser: data.id_user,
      idProject: data.id_project,
      createdAt: {
        $gte: new Date(todayStart.getFullYear(), todayStart.getMonth(), todayStart.getDate(), 0, 0, 0, 0),
        $lt: new Date(todayEnd.getFullYear(), todayEnd.getMonth(), todayEnd.getDate(), 23, 59, 59, 999),
      },
    });

    // si un modèle existe
    if (stats) {
      // on le met à jour en l'incrémentant
      stats.folders = action === "CREATE" ? stats.folders + 1 : stats.folders - 1;
    } else {
      // sinon on crée le modèle pour le jour
      stats = new Stats({
        idUser: data.id_user,
        idProject: data.id_project,
        folders: action === "CREATE" ? 1 : -1,
      });
    }

    // on met à jour les données
    await stats.save();
  } catch (error) {
    console.log("error: ", error);
    return;
  }
};

const miseAJourDesSessions = async (data: IJoinRoom, id_session: string) => {
  try {
    // ##########################################################
    //        On récupère le modèle des stats s'il existe
    // ##########################################################
    const todayStart = new Date();
    const todayEnd = new Date();
    let stats: any | null = await Stats.findOne({
      idUser: data.id_user,
      idProject: data.id_project,
      createdAt: {
        $gte: new Date(todayStart.getFullYear(), todayStart.getMonth(), todayStart.getDate(), 0, 0, 0, 0),
        $lt: new Date(todayEnd.getFullYear(), todayEnd.getMonth(), todayEnd.getDate(), 23, 59, 59, 999),
      },
    });

    // on récupère la valeur ou une tableau vide si null
    const streamSessions = stats?.streamSessions || [];
    // on ajoute l'id session
    streamSessions.push(id_session);

    // si un modèle existe
    if (stats) {
      stats.streamSessions = streamSessions;
    } else {
      // sinon on crée le modèle pour le jour
      stats = new Stats({
        idUser: data.id_user,
        idProject: data.id_project,
        streamSessions,
      });
    }

    // on met à jour les données
    await stats.save();
  } catch (error) {
    console.log("error: ", error);
    return;
  }
};
