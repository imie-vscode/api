import { Request, Response } from "express";
import User from "../../models/User";
import { IAdminRegisterRequestBody, IadminLoginRequestBody } from "../../interfaces/admin/auth.interface";
import bcrypt from "bcrypt";
import { createToken, getToken, IToken } from "../../utils/token";
import { messageEmail, sendMail } from "../../utils/email";
import removeAccountTemplate from "../../templates/removeAccountTemplate";
import { IUpdateEmployeRequestBody } from "../../interfaces/admin/employe.interface";

export const adminGetProfile = async (req: Request, res: Response) => {
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const _id = req.params.id_user;

  if (token._id !== _id) {
    return res.status(403).json({ error: "Non authentifié" });
  }else{
    if (token.role !== "ADMIN" , token.role !== "EMPLOYE"){
      return res.status(403).json({ error: "Vous n'avez pas les droits pour effectuer cette action" });
    }
  try {
    const user: any = await User.findById(token._id);
    if (!user) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }

    const userData = {
      firstname: user.firstname,
      lastname: user.lastname,
      pseudo: user.pseudo,
      email: user.email,
      address: user.address,
      numAddress: user.numAddress,
      postalCode: user.postalCode,
      country: user.country,
      sexe: user.sexe,
      birthday: new Date(user.birthday),
      role: user.role,
    };

    res.status(200).json(userData);
  } catch (error) {
    console.log("error: ", error);
    res.status(500).json({ error: "Erreur serveur" });
  }
}};

export const adminUpdateProfile = async (req: Request, res: Response) => {
  const body = req.body as IUpdateEmployeRequestBody;
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const _id = req.params.id_user;

  if (token._id !== _id) {
    return res.status(403).json({ error: "Non authentifié" });
  }else{
    if (token.role !== "ADMIN" , token.role !== "EMPLOYE"){
      return res.status(403).json({ error: "Vous n'avez pas les droits pour effectuer cette action" });
    }
  try {
    const user = await User.findById(token._id);
    if (!user) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }

    const userData = {
      firstname: body.firstname,
      lastname: body.lastname,
      pseudo: body.pseudo,
      address: body.address,
      numAddress: body.num_address,
      postalCode: body.postal_code,
      country: body.country,
      sexe: body.sexe,
      birthday: body.birthday,
      role: body.role,
    };

    await user.set(userData).save();

    res.status(201).json({ message: "Utilisateur modifié avec succès" });
  } catch (error) {
    console.log("error: ", error);
    res.status(500).json({ error: "Erreur serveur" });
  }
}};

export const adminRemoveProfile = async (req: Request, res: Response) => {
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const _id = req.params.id_user;

  if (token._id !== _id) {
    return res.status(403).json({ error: "Non authentifié" });
  }
  try {
    const user = await User.findById(token._id);
    if (!user) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }
    // on anonymise le profil en supprimant ses informations sauf les données relatives aux factures
    const userData: any = await user.set({ active: false }).save();
    // on supprime ses images
    // on envoie un courriel pour signaler la bonne supression des données
    const message = messageEmail(
      userData.email,
      "Suppression de votre compte IMIE VSCode",
      "Suppression réussie !",
      removeAccountTemplate(userData.firstname, userData.lastname)
    );

    sendMail(message);

    res.status(201).json({ message: "Profil supprimé avec succès" });
  } catch (error) {
    console.log("error: ", error);
    res.status(500).json({ error: "Erreur serveur" });
  }
};
