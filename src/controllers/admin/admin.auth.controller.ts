import { Request, Response } from "express";
import User from "../../models/User"
import { IAdminRegisterRequestBody, IadminLoginRequestBody } from "../../interfaces/admin/auth.interface";
import bcrypt from "bcrypt";
import { createToken } from "../../utils/token";
import { sendMail, messageEmail } from "../../utils/email";
import registerTemplate from "../../templates/registerTemplate";

export const adminRegisterController = async (req: Request, res: Response) => {
  const body = req.body as IAdminRegisterRequestBody;
  try {
    const emailExist = await User.findOne({ email: body.email });
    // si le courriel existe, on retourne une erreur
    if (emailExist) {
      return res.status(400).json({ error: "Courriel déjà existant" });
    }

    // hash du mot de passe
    const hash = await bcrypt.hash(body.password, 10);

    // insertion des données
    const userData = {
      email: body.email,
      firstname: body.firstname,
      lastname: body.lastname.toUpperCase(),
      pseudo: body.pseudo,
      password: hash,
      address: body.address,
      numAddress: body.num_address,
      postalCode: body.postal_code,
      country: body.country.toUpperCase(),
      sexe: body.sexe,
      birthday: new Date(body.birthday),
      role: body.role,
    };
    const userSaved: any = await new User(userData).save();
    // création du token
    const token = createToken(userSaved);
    if (!token) {
      return res.status(500).json({ error: "Erreur serveur, veuillez vous connecter manuellement" });
    }

    // formattage des données pour la réponse
    const user = {
      firstname: userSaved.firstname,
      lastname: userSaved.lastname,
      pseudo: userSaved.pseudo,
      email: userSaved.email,
      _id: userSaved._id,
    };

    const message = messageEmail(
      user.email,
      "Inscription sur la plateforme IMIE VSCode ✔",
      "Inscription validée !",
      registerTemplate(user.firstname, user.lastname)
    );

    sendMail(message);

    res.status(201).json({ message: "Inscription validée", token, user });
  } catch (error) {
    console.log("error: ", error);
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const adminLoginController = async (req: Request, res: Response) => {
  const body = req.body as IadminLoginRequestBody;
  try {
    const userExist: any = await User.findOne({ email: body.email, active: true });
    // si le courriel existe, on retourne une erreur
    if (!userExist) {
      return res.status(409).json({ error: "Couple courriel/mot de passe incorrect" });
    }

    // hash du mot de passe
    const goodPassword = await bcrypt.compare(body.password, userExist.password);
    if (!goodPassword) {
      return res.status(409).json({ error: "Couple courriel/mot de passe incorrect" });
    }

    // suppression de la référence en faisant un parse stringify pour pouvoir supprimer une clé de l'objet

    // création du token
    const token = createToken(userExist);
    if (!token) {
      return res.status(500).json({ error: "Erreur serveur, veuillez réessayer" });
    }
    // formattage des données pour la réponse
    const user = {
      firstname: userExist.firstname,
      lastname: userExist.lastname,
      pseudo: userExist.pseudo,
      email: userExist.email,
      _id: userExist._id,
    };

    res.status(200).json({ token, user });
  } catch (error) {
    console.log("error: ", error);
    res.status(500).json({ error: "Erreur serveur" });
  }
};



