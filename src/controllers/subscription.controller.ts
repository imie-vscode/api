import { Request, Response } from "express";
import { errorLogger } from "../config/winston";
import { ICreateSubscriptionRequestBody } from "../interfaces/subscription.interface";
import Subscription from "../models/Subscription";
import { formatPrice } from "../utils/formators";
import { parseUserAgent } from "../utils/parsers";

//TODO: formatter les prix au cas où

export const getAllSubscriptions = async (req: Request, res: Response) => {
  try {
    // on récupères les abonnements en excluants une valeur inutile
    const subscriptions = await Subscription.find({}, { __v: 0 }).sort("asc");

    res.status(200).json(subscriptions);
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/subscription.controller => getAllSubscriptions] - ${error.message} - ${req.originalUrl} - ${
        req.method
      } - ${req.ip} - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const getSubscription = async (req: Request, res: Response) => {
  const id_subscription = req.params.id_subscription;

  // si l'id n'est pas renseigné on retourne une erreur afin d'éviter de faire une requête ou que sa taille est incorrecte
  if (!id_subscription || id_subscription?.length !== 24) {
    return res.status(404).json({ error: "Abonnement introuvable" });
  }

  try {
    // on vérifie si l'abonnement existe
    const subscription = await Subscription.findById(id_subscription, { __v: 0 });
    if (!subscription) {
      return res.status(404).json({ error: "Abonnement introuvable" });
    }

    res.status(200).json(subscription);
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/subscription.controller => getSubscription] - ${error.message} - ${req.originalUrl} - ${
        req.method
      } - ${req.ip} - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const createSubscription = async (req: Request, res: Response) => {
  const body = req.body as ICreateSubscriptionRequestBody;
  try {
    // on vérifie que l'abonnement n'est pas déjà existante
    const subscriptionExist = await Subscription.findOne({ name: body.name });
    if (subscriptionExist) {
      return res.status(400).json({ error: "Abonnement déjà existant" });
    }

    const subscriptionData = {
      name: body.name,
      description: body.description,
      price: formatPrice(body.price),
      advantages: body.advantages || [],
    };

    // on insère l'abonnement en base de données
    await new Subscription(subscriptionData).save();
    res.status(201).json({ message: "Abonnement inséré avec succès !" });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/subscription.controller => createSubscription] - ${error.message} - ${req.originalUrl} - ${
        req.method
      } - ${req.ip} - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const updateSubscription = async (req: Request, res: Response) => {
  const body = req.body as ICreateSubscriptionRequestBody;
  const id_subscription = req.params.id_subscription;

  // si l'id n'est pas renseigné on retourne une erreur afin d'éviter de faire une requête
  if (!id_subscription || id_subscription?.length !== 24) {
    return res.status(404).json({ error: "Abonnement introuvable" });
  }

  try {
    // on vérifie si l'abonnement existe
    const subscription: any = await Subscription.findById(id_subscription);
    if (!subscription) {
      return res.status(404).json({ error: "Abonnement introuvable" });
    }

    // on vérifie si le nom entré est différent de celui en bdd
    if (body.name !== (subscription.name as string)) {
      // s'il est diffenent alors on vérifie qu'il n'existe pas en bdd
      const newSubscriptionNameExist = await Subscription.findOne({ name: body.name });
      if (newSubscriptionNameExist) {
        return res.status(400).json({ error: "Abonnement déjà existant" });
      }
    }

    const subscriptionData = {
      name: body.name,
      description: body.description,
      price: formatPrice(body.price),
      advantages: body.advantages || [],
    };

    // on modifie l'abonnement en base de données
    await subscription.set(subscriptionData).save();

    res.status(201).json({ message: "Abonnement modifié avec succès !" });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/subscription.controller => updateSubscription] - ${error.message} - ${req.originalUrl} - ${
        req.method
      } - ${req.ip} - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const deleteSubscription = async (req: Request, res: Response) => {
  const id_subscription = req.params.id_subscription;

  // si l'id n'est pas renseigné on retourne une erreur afin d'éviter de faire une requête
  if (!id_subscription || id_subscription?.length !== 24) {
    return res.status(404).json({ error: "Abonnement introuvable" });
  }

  try {
    // on vérifie si l'abonnement existe
    const subscription = await Subscription.findById(id_subscription);
    if (!subscription) {
      return res.status(404).json({ error: "Abonnement introuvable" });
    }

    await subscription.delete();
    res.status(201).json({ message: "Abonnement supprimé avec succès !" });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/subscription.controller => deleteSubscription] - ${error.message} - ${req.originalUrl} - ${
        req.method
      } - ${req.ip} - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};
