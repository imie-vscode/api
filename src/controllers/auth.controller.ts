require("dotenv").config();
import { Request, Response } from "express";
import User from "../models/User";
import {
  IRegisterRequestBody,
  ILoginRequestBody,
  IForgotPasswordRequestBody,
  IResetPasswordRequestBody,
  IRegisterWithGoogleRequestBody,
  ILoginWithGoogleRequestBody,
  ILoginWithFacebookRequestBody,
  IRegisterWithFacebookRequestBody,
  IRegisterWithGithubRequestBody,
} from "../interfaces/auth.interface";
import bcrypt from "bcrypt";
import { createToken } from "../utils/token";
import { sendMail, messageEmail } from "../utils/email";
import registerTemplate from "../templates/registerTemplate";
import Subscription from "../models/Subscription";
import randomstring from "randomstring";
import forgotPasswordTemplate from "../templates/forgotpasswordTemplate";
import { ILoginWithGithubRequestBody } from "../interfaces/auth.interface";
import fs from "fs";
import { errorLogger } from "../config/winston";
import { parseUserAgent } from "../utils/parsers";

export const registerController = async (req: Request, res: Response) => {
  const body = req.body as IRegisterRequestBody;
  try {
    const emailExist = await User.findOne({ email: body.email });
    // si le courriel existe, on retourne une erreur
    if (emailExist) {
      return res.status(400).json({ error: "Courriel déjà existant" });
    }

    // hash du mot de passe
    const hash = await bcrypt.hash(body.password, 10);

    // on ajoute un abonnement gratuit par défault avec un regex qui prend le texte sans la casse, au cas où on change le nom
    // en récupérant que les champs nécessaires
    const subscription = await Subscription.findOne({ name: /^gratuit$/i }, { createdAt: 0, updatedAt: 0, _id: 0, __v: 0 });

    // insertion des données
    const userData = {
      email: body.email,
      firstname: body.firstname,
      lastname: body.lastname.toUpperCase(),
      pseudo: body.pseudo,
      password: hash,
      city: body.city,
      address: body.address,
      numAddress: body.num_address,
      postalCode: body.postal_code,
      country: body.country.toUpperCase(),
      sexe: body.sexe,
      birthday: new Date(body.birthday),
      newsletter: Boolean(body.newsletter),
      subscription,
    };
    const userSaved: any = await new User(userData).save();
    // création du token
    const token = createToken(userSaved);
    if (!token) {
      return res.status(500).json({ error: "Erreur serveur, veuillez vous connecter manuellement" });
    }

    // création du dossier utilisateur
    const pathname = `./projects/${userSaved._id}/`;
    fs.mkdirSync(pathname);

    // formattage des données pour la réponse
    const user = JSON.parse(JSON.stringify(userSaved));
    delete user.__v;
    delete user.password;
    delete user.role;
    delete user.active;

    const message = messageEmail(
      user.email,
      "Inscription sur la plateforme IMIE VSCode ✔",
      "Inscription validée !",
      registerTemplate(user.lastname, user.firstname)
    );

    sendMail(message);

    res.status(201).json({ message: "Inscription validée", token, user });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/auth.controller => registerController] - ${error.message} - ${req.originalUrl} - ${req.method} - ${
        req.ip
      } - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const loginController = async (req: Request, res: Response) => {
  const body = req.body as ILoginRequestBody;
  try {
    const userExist: any = await User.findOne({ email: body.email, active: true });
    // si le courriel existe, on retourne une erreur
    if (!userExist) {
      return res.status(409).json({ error: "Couple courriel/mot de passe incorrect" });
    }

    // hash du mot de passe
    const goodPassword = await bcrypt.compare(body.password, userExist.password);
    if (!goodPassword) {
      return res.status(409).json({ error: "Couple courriel/mot de passe incorrect" });
    }

    // suppression de la référence en faisant un parse stringify pour pouvoir supprimer une clé de l'objet

    // création du token
    const token = createToken(userExist);
    if (!token) {
      return res.status(500).json({ error: "Erreur serveur, veuillez réessayer" });
    }
    // formattage des données pour la réponse
    const user = JSON.parse(JSON.stringify(userExist));
    delete user.__v;
    delete user.password;
    delete user.role;
    delete user.active;

    res.status(200).json({ token, user });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/auth.controller => loginController] - ${error.message} - ${req.originalUrl} - ${req.method} - ${
        req.ip
      } - ${parseUserAgent(req)}`
    );

    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const forgotPasswordController = async (req: Request, res: Response) => {
  const body = req.body as IForgotPasswordRequestBody;

  try {
    // on génére une chaîne de caractères aléatoire
    const str = randomstring.generate({
      length: 48,
      charset: "alphanumeric",
    });
    // on vérifie le courriel en base de données
    const userExist: any = await User.findOne({ email: body.email });
    if (!userExist) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }

    // on stocke la chaîne de caractères dans le cache en l'associant avec le courriel de l'utilisateur
    myCache.set(str, String(userExist.email), 900); // 15min

    // on envoie un courriel avec le lien
    const siteUrl = `${process.env.SITE_URL}/reinitialiser-mon-mot-de-passe?str=${str}`;
    const message = messageEmail(
      userExist.email,
      "Réinitialisation de votre mot de passe IMIE VSCode",
      `URL de réinitialisation de votre mot de passe : ${siteUrl}`,
      forgotPasswordTemplate(siteUrl)
    );

    const mailSent = await sendMail(message);
    if (!mailSent) {
      return res.status(500).json({ error: "Erreur survenue lors de l'envoi du courriel, veuillez réessayer" });
    }

    res.status(200).json({ message: "Un courriel vous a été envoyé sur votre adresse électronique" });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/auth.controller => forgotPasswordController] - ${error.message} - ${req.originalUrl} - ${
        req.method
      } - ${req.ip} - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const resetPasswordController = async (req: Request, res: Response) => {
  const body = req.body as IResetPasswordRequestBody;

  try {
    const str = body.str;
    const email = myCache.get(str);
    // si on ne trouve pas le courriel c'est que le line a expiré (15min) ou que la chaîne de caractères n'est pas bonne
    // on notifie à l'utilisateur que le lien a été expiré par défaut
    if (!email) {
      return res.status(404).json({ error: "Lien expiré. Veuillez réessayer" });
    }

    // on vérifie le courriel en base de données
    const user: any = await User.findOne({ email });
    if (!user) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }

    // hash du mot de passe
    const hash = await bcrypt.hash(body.password, 10);

    // mise à jour du nouveau mot de passe
    await user.set({ password: hash }).save();

    // suppression de l'ancienne valeur du cache quand le mot de passe est mis à jour
    myCache.del(str);

    res.status(201).json({ message: "Votre mot de passe a été mis à jour !" });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/auth.controller => resetPasswordController] - ${error.message} - ${req.originalUrl} - ${
        req.method
      } - ${req.ip} - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

// ####################################################################################################################
//                                              FACEBOOK
// ####################################################################################################################

export const registerWithFacebookController = async (req: Request, res: Response) => {
  try {
    const body = req.body as IRegisterWithFacebookRequestBody;

    // ##########################################################
    //    Vérification si le courriel de l'utilisateur existe
    // ##########################################################
    const emailExist = await User.findOne({ email: body.email });
    if (emailExist) {
      return res.status(400).json({ error: "Courriel déjà existant" });
    }

    // ##########################################################
    //  Création d'un nouveau modèle utilisateur avec les infos
    // ##########################################################

    // on hash l'id facebook de l'utilisateur en guise de mot de passe
    const hash = await bcrypt.hash(body.facebookId, 10);

    const userData = {
      facebookId: body.facebookId,
      firstname: body.firstname,
      lastname: body.lastname,
      sexe: body.sexe,
      email: body.email,
      birthday: new Date(body.birthday),
      pseudo: body.pseudo,
      address: body.address,
      numAddress: body.num_address,
      postalCode: body.postal_code,
      city: body.city,
      country: body.country,
      password: hash,
      newsletter: Boolean(body.newsletter),
    };

    const userSaved: any = await new User(userData).save();

    // création du token
    const token = createToken(userSaved);

    // formattage des données pour la réponse
    const user = JSON.parse(JSON.stringify(userSaved));
    delete user.__v;
    delete user.password;
    delete user.role;
    delete user.active;

    const message = messageEmail(
      user.email,
      "Inscription sur la plateforme IMIE VSCode ✔",
      "Inscription validée !",
      registerTemplate(user.lastname, user.firstname)
    );

    sendMail(message);

    res.status(201).json({ message: "Inscription validée", token, user });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/auth.controller => registerWithFacebookController] - ${error.message} - ${req.originalUrl} - ${
        req.method
      } - ${req.ip} - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const loginWithFacebookController = async (req: Request, res: Response) => {
  try {
    const body = req.body as ILoginWithFacebookRequestBody;

    // ##########################################################
    //  Vérification si l'utilisateur est inscrit avec Facebook
    // ##########################################################
    const userWithFacebookId: any = await User.findOne({ facebookId: body.facebookId, email: body.email });
    // s'il existe, on envoie un token à l'utilisateur car cela veut dire qu'il est déjà inscrit
    if (userWithFacebookId) {
      const token = createToken(userWithFacebookId);
      if (!token) {
        return res.status(500).json({ error: "Erreur serveur, veuillez réessayer" });
      }
      // formattage des données pour la réponse
      const user = JSON.parse(JSON.stringify(userWithFacebookId));
      delete user.__v;
      delete user.password;
      delete user.role;
      delete user.active;

      return res.status(200).json({ token, user });
    }

    const emailExist = await User.findOne({ email: body.email });
    if (emailExist) {
      return res.status(400).json({ error: "Courriel déjà existant. Inscrivez-vous manuellement avec un autre courriel" });
    }

    // sinon on retourne comme quoi il n'est pas inscrit donc il devra compléter des informations pour valider son inscription afin qu'il puisse se connecter
    res.status(404).json({ error: "Utilisateur introuvable" });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/auth.controller => loginWithFacebookController] - ${error.message} - ${req.originalUrl} - ${
        req.method
      } - ${req.ip} - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

// ####################################################################################################################
//                                              GOOGLE
// ####################################################################################################################

export const loginWithGoogleController = async (req: Request, res: Response) => {
  try {
    const body = req.body as ILoginWithGoogleRequestBody;

    // ##########################################################
    //  Vérification si l'utilisateur est inscrit avec Google
    // ##########################################################
    const userWithGoogleId: any = await User.findOne({ googleId: body.googleId, email: body.email });
    // s'il existe, on envoie un token à l'utilisateur car cela veut dire qu'il est déjà inscrit
    if (userWithGoogleId) {
      const token = createToken(userWithGoogleId);
      if (!token) {
        return res.status(500).json({ error: "Erreur serveur, veuillez réessayer" });
      }
      // formattage des données pour la réponse
      const user = JSON.parse(JSON.stringify(userWithGoogleId));
      delete user.__v;
      delete user.password;
      delete user.role;
      delete user.active;

      return res.status(200).json({ token, user });
    }

    const emailExist = await User.findOne({ email: body.email });
    if (emailExist) {
      return res.status(400).json({ error: "Courriel déjà existant. Inscrivez-vous manuellement avec un autre courriel" });
    }

    // sinon on retourne comme quoi il n'est pas inscrit donc il devra compléter des informations pour valider son inscription afin qu'il puisse se connecter
    res.status(404).json({ error: "Utilisateur introuvable" });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/auth.controller => loginWithGoogleController] - ${error.message} - ${req.originalUrl} - ${
        req.method
      } - ${req.ip} - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const registerWithGoogleController = async (req: Request, res: Response) => {
  try {
    const body = req.body as IRegisterWithGoogleRequestBody;

    // ##########################################################
    //    Vérification si le courriel de l'utilisateur existe
    // ##########################################################
    const emailExist = await User.findOne({ email: body.email });
    if (emailExist) {
      return res.status(400).json({ error: "Courriel déjà existant" });
    }

    // ##########################################################
    //  Création d'un nouveau modèle utilisateur avec les infos
    // ##########################################################

    // on hash l'id facebook de l'utilisateur en guise de mot de passe
    const hash = await bcrypt.hash(body.googleId, 10);

    const userData = {
      googleId: body.googleId,
      firstname: body.firstname,
      lastname: body.lastname,
      sexe: body.sexe,
      email: body.email,
      birthday: new Date(body.birthday),
      pseudo: body.pseudo,
      address: body.address,
      numAddress: body.num_address,
      postalCode: body.postal_code,
      city: body.city,
      country: body.country,
      password: hash,
      newsletter: Boolean(body.newsletter),
    };

    const userSaved: any = await new User(userData).save();
    console.log("userSaved: ", userSaved);

    // création du token
    const token = createToken(userSaved);

    // formattage des données pour la réponse
    const user = JSON.parse(JSON.stringify(userSaved));
    delete user.__v;
    delete user.password;
    delete user.role;
    delete user.active;

    const message = messageEmail(
      user.email,
      "Inscription sur la plateforme IMIE VSCode ✔",
      "Inscription validée !",
      registerTemplate(user.lastname, user.firstname)
    );

    sendMail(message);

    res.status(201).json({ message: "Inscription validée", token, user });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/auth.controller => registerWithGoogleController] - ${error.message} - ${req.originalUrl} - ${
        req.method
      } - ${req.ip} - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

// ####################################################################################################################
//                                              GITHUB
// ####################################################################################################################

export const loginWithGithubController = async (req: Request, res: Response) => {
  try {
    const body = req.body as ILoginWithGithubRequestBody;

    // ##########################################################
    //  Vérification si l'utilisateur est inscrit avec Github
    // ##########################################################
    const userWithGithubId: any = await User.findOne({ githubId: body.githubId, email: body.email });
    // s'il existe, on envoie un token à l'utilisateur car cela veut dire qu'il est déjà inscrit
    if (userWithGithubId) {
      const token = createToken(userWithGithubId);
      if (!token) {
        return res.status(500).json({ error: "Erreur serveur, veuillez réessayer" });
      }
      // formattage des données pour la réponse
      const user = JSON.parse(JSON.stringify(userWithGithubId));
      delete user.__v;
      delete user.password;
      delete user.role;
      delete user.active;

      return res.status(200).json({ token, user });
    }

    const emailExist = await User.findOne({ email: body.email });
    if (emailExist) {
      return res.status(400).json({ error: "Courriel déjà existant. Inscrivez-vous manuellement avec un autre courriel" });
    }

    // sinon on retourne comme quoi il n'est pas inscrit donc il devra compléter des informations pour valider son inscription afin qu'il puisse se connecter
    res.status(404).json({ error: "Utilisateur introuvable" });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/auth.controller => loginWithGithubController] - ${error.message} - ${req.originalUrl} - ${
        req.method
      } - ${req.ip} - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const registerWithGithubController = async (req: Request, res: Response) => {
  try {
    const body = req.body as IRegisterWithGithubRequestBody;

    // ##########################################################
    //    Vérification si le courriel de l'utilisateur existe
    // ##########################################################
    const emailExist = await User.findOne({ email: body.email });
    if (emailExist) {
      return res.status(400).json({ error: "Courriel déjà existant" });
    }

    // ##########################################################
    //  Création d'un nouveau modèle utilisateur avec les infos
    // ##########################################################

    // on hash l'id facebook de l'utilisateur en guise de mot de passe
    const hash = await bcrypt.hash(body.githubId, 10);

    const userData = {
      githubId: body.githubId,
      firstname: body.firstname,
      lastname: body.lastname,
      sexe: body.sexe,
      email: body.email,
      birthday: new Date(body.birthday),
      pseudo: body.pseudo,
      address: body.address,
      numAddress: body.num_address,
      postalCode: body.postal_code,
      city: body.city,
      country: body.country,
      password: hash,
      newsletter: Boolean(body.newsletter),
    };

    const userSaved: any = await new User(userData).save();

    // création du token
    const token = createToken(userSaved);

    // formattage des données pour la réponse
    const user = JSON.parse(JSON.stringify(userSaved));
    delete user.__v;
    delete user.password;
    delete user.role;
    delete user.active;

    const message = messageEmail(
      user.email,
      "Inscription sur la plateforme IMIE VSCode ✔",
      "Inscription validée !",
      registerTemplate(user.lastname, user.firstname)
    );

    sendMail(message);

    res.status(201).json({ message: "Inscription validée", token, user });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/auth.controller => registerWithGithubController] - ${error.message} - ${req.originalUrl} - ${
        req.method
      } - ${req.ip} - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};
