require("dotenv").config();
import { Request, Response } from "express";
import User from "../models/User";
import { getToken, IToken } from "../utils/token";
import {
  IAddUserInProjectRequestBody,
  INewProjectRequestBody,
  IUpdateUserRequestBody,
  IUpdateUserSubscriptionRequestBody,
} from "../interfaces/user.interface";
import { messageEmail, sendMail } from "../utils/email";
import fs from "fs-extra";
import removeAccountTemplate from "../templates/removeAccountTemplate";
import getPersonnalDataTemplate from "../templates/getPersonnalDataTemplate";
import Subscription from "../models/Subscription";
import resetSubscriptionTemplate from "../templates/resetSubscriptionTemplate";
import updateSubscriptionTemplate from "../templates/updateSubscriptionTemplate";
import { createSubscription, removeSubscription } from "../utils/stripe";
import Stripe from "stripe";
import PdfGenerator from "../utils/PdfGenerator";
import puppeteer from "puppeteer";
import randomstring from "randomstring";
import { __LOGO_PATH } from "../templates/variables";
import moment from "moment";
import { formatPrice } from "../utils/formators";
import Bill from "../models/Bill";
import getBillTemplate from "../templates/getBillTemplate";
import "moment/locale/fr";
moment.locale("fr");
import Zip from "node-zip";
import Project from "../models/Project";
import { EXTENSIONS } from "../utils/constantes";
import dirTree from "directory-tree";
import Stats from "../models/Stats";
import { errorLogger } from "../config/winston";
import { parseUserAgent } from "../utils/parsers";

//
// PARTIE UTILISATEUR
//
export const getProfile = async (req: Request, res: Response) => {
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const _id = req.params.id_user;

  if (token._id !== _id) {
    return res.status(403).json({ error: "Non authentifié" });
  }
  try {
    const user = await User.findById(token._id, { role: 0, active: 0, password: 0, __v: 0 });
    if (!user) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }

    res.status(200).json(user);
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/user.controller => getProfile] - ${error.message} - ${req.originalUrl} - ${req.method} - ${
        req.ip
      } - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const updateProfile = async (req: Request, res: Response) => {
  const body = req.body as IUpdateUserRequestBody;
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const _id = req.params.id_user;

  if (token._id !== _id) {
    return res.status(403).json({ error: "Non authentifié" });
  }
  try {
    const userExist = await User.findById(token._id);
    if (!userExist) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }

    const userData = {
      firstname: body.firstname,
      lastname: body.lastname,
      pseudo: body.pseudo,
      address: body.address,
      numAddress: body.num_address,
      postalCode: body.postal_code,
      country: body.country,
      sexe: body.sexe,
      birthday: body.birthday,
      city: body.city,
    };

    const userSaved: any = await userExist.set(userData).save();

    // on supprime les données inutiles
    const user = JSON.parse(JSON.stringify(userSaved));
    delete user.__v;
    delete user.password;
    delete user.role;
    delete user.active;

    res.status(201).json({ message: "Profil modifié avec succès !", user });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/user.controller => updateProfile] - ${error.message} - ${req.originalUrl} - ${req.method} - ${
        req.ip
      } - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const removeProfile = async (req: Request, res: Response) => {
  const query = req.query as { personnal_data?: "checked" | "unchecked" };

  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const _id = req.params.id_user;

  const zip = new Zip();

  if (token._id !== _id) {
    return res.status(403).json({ error: "Non authentifié" });
  }
  try {
    const user: any = await User.findById(token._id);
    if (!user) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }

    // on ajoute un abonnement gratuit par défault avec un regex qui prend le texte sans la casse, au cas où on change le nom
    // en récupérant que les champs nécessaires
    const subscription: any = await Subscription.findOne({ name: /^gratuit$/i }, { createdAt: 0, updatedAt: 0, _id: 0, __v: 0 });
    // si jamais l'abonnement gratuit n'est pas trouvé, ce qui ne doit jamais arriver
    if (!subscription) {
      return res.status(500).json({ error: "Erreur serveur" });
    }

    // on supprime l'abonnement s'il en a un
    //si l'abonnement actuel n'est pas gratuit et qu'un idStripe est renseigné (vérif), on doit se désabonner de l'ancien
    if (!/^gratuit$/i.test(user.subscription.name) && Boolean(user.idStripe)) {
      try {
        await removeSubscription(user.idStripe);
      } catch (error) {
        console.log("error: ", error);
        switch (error.raw.code as string) {
          case "resource_missing":
            return res.status(400).json({ error: "Abonnement déjà supprimé" });
          default:
            return res.status(500).json({ error: "Erreur survenue lors du paiement, veuillez réessayer" });
        }
      }
    }

    const update = {
      subscription: {
        name: subscription.name,
        description: subscription.description,
        price: subscription.price,
        advantages: subscription.advantages,
        subscribedSince: new Date().toISOString(),
      },
      idStripe: null,
      active: false,
      newsletter: false,
    };
    // on anonymise le profil en supprimant ses informations sauf les données relatives aux factures
    const userData: any = await user.set(update).save();
    // on supprime ses images
    // on récupères les données utilisateurs s'il a fait le choix de récupérer ses données (RGPD)

    let dataZiped = null;
    if (query.personnal_data === "checked") {
      // on récupères les données utilisateurs (RGPD)
      const allUserData = { user };

      // on récupère les factures de l'utilisateurs
      const bills: any[] = await Bill.find({ idUser: token._id })
        .populate({
          path: "idUser",
          model: "users",
        })
        .populate({
          path: "idSubscription",
          model: "subscriptions",
        });

      // on ajoute les factures dans une archive
      for (let i = 0; i < bills.length; i++) {
        const createdAt = new Date(bills[i].createdAt).getTime();
        const month = moment(createdAt).format("MM");
        const year = moment(createdAt).format("YYYY");

        const billPdf = await generateBill(
          bills[i].idSubscription.price,
          bills[i].idSubscription.name,
          bills[i].idUser,
          bills[i].idStripe,
          createdAt
        );
        zip.file(`(${i + 1})_IMIEVSCode_facture_${month}_${year}.pdf`, billPdf);
      }

      // on ajoute les données utilisateurs
      zip.file("donnees_utilisateur.txt", JSON.stringify(allUserData, null, 2));

      // on zip tous les fichiers
      dataZiped = zip.generate({ base64: false, compression: "DEFLATE" });
    }
    // fichier que l'on va envoyer avec le mail de suppression de compte
    // si l'utilisateur souhaite récupérer ses données, on ajoute un fichier sinon on renseigne undefined pour pas envoyer de fichiers
    const attachments =
      query.personnal_data === "checked"
        ? [
            {
              filename: "donnees_utilisateur.zip",
              content: dataZiped,
              encoding: "binary",
            },
          ]
        : undefined;

    // on envoie un courriel pour signaler la bonne supression des données
    const message = messageEmail(
      userData.email,
      "Suppression de votre compte IMIE VSCode",
      "Suppression réussie !",
      removeAccountTemplate(userData.lastname, userData.firstname),
      attachments
    );

    sendMail(message);

    res.status(201).json({ message: "Profil supprimé avec succès" });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/user.controller => removeProfile] - ${error.message} - ${req.originalUrl} - ${req.method} - ${
        req.ip
      } - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const getpersonnalData = async (req: Request, res: Response) => {
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const _id = req.params.id_user;
  const zip = new Zip();

  if (token._id !== _id) {
    return res.status(403).json({ error: "Non authentifié" });
  }
  try {
    const user: any = await User.findById(token._id);
    if (!user) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }

    // on récupères les données utilisateurs (RGPD)
    const allUserData = { user };

    // on récupère les factures de l'utilisateurs
    const bills: any[] = await Bill.find({ idUser: token._id })
      .populate({
        path: "idUser",
        model: "users",
      })
      .populate({
        path: "idSubscription",
        model: "subscriptions",
      });

    // on ajoute les factures dans une archive
    for (let i = 0; i < bills.length; i++) {
      const createdAt = new Date(bills[i].createdAt).getTime();
      const month = moment(createdAt).format("MM");
      const year = moment(createdAt).format("YYYY");

      const billPdf = await generateBill(bills[i].idSubscription.price, bills[i].idSubscription.name, bills[i].idUser, bills[i].idStripe, createdAt);
      zip.file(`(${i + 1})_IMIEVSCode_facture_${month}_${year}.pdf`, billPdf);
    }

    // on ajoute les données utilisateurs
    zip.file("donnees_utilisateur.txt", JSON.stringify(allUserData, null, 2));

    // on zip tous les fichiers
    const dataZiped = zip.generate({ base64: false, compression: "DEFLATE" });

    // on ajoute l'archive au courriel
    const attachments = [
      {
        filename: "donnees_utilisateur.zip",
        content: dataZiped,
        encoding: "binary",
      },
    ];

    // on envoie un courriel pour signaler la bonne supression des données
    const message = messageEmail(
      user.email,
      "Récupération de vos données personnelles IMIE VSCode",
      "Récupération de vos données personnelles IMIE VSCode se trouvant en pièces jointes de ce courriel",
      getPersonnalDataTemplate(user.lastname, user.firstname),
      attachments
    );

    const mailSent = await sendMail(message);
    if (!mailSent) {
      return res.status(500).json({ error: "Erreur lors de l'envoi du courriel, veuillez réessayer plus tard" });
    }

    res.status(200).json({ message: "Un courriel vous a été envoyé avec vos données personnelles en pièces jointes" });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/user.controller => getpersonnalData] - ${error.message} - ${req.originalUrl} - ${req.method} - ${
        req.ip
      } - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const getBills = async (req: Request, res: Response) => {
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const _id = req.params.id_user;

  if (token._id !== _id) {
    return res.status(403).json({ error: "Non authentifié" });
  }
  try {
    const bills = await Bill.find({ idUser: token._id }).populate({
      path: "idUser",
      model: "users",
      populate: {
        path: "subscription",
        model: "subscriptions",
      },
    });

    res.status(200).json(bills);
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/user.controller => getBills] - ${error.message} - ${req.originalUrl} - ${req.method} - ${
        req.ip
      } - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const getBill = async (req: Request, res: Response) => {
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const _id = req.params.id_user;
  const idBill = req.params.id_bill as string;

  if (token._id !== _id) {
    return res.status(403).json({ error: "Non authentifié" });
  }
  if (!idBill) {
    return res.status(404).json({ error: "Facture introuvable" });
  }

  try {
    const bill = await Bill.findById(idBill)
      .populate({
        path: "idUser",
        model: "users",
      })
      .populate({
        path: "idSubscription",
        model: "subscriptions",
      });
    if (!bill) {
      return res.status(404).json({ error: "Facture introuvable" });
    }

    const billData = JSON.parse(JSON.stringify(bill));
    delete billData.idUser.password;
    delete billData.idUser.active;
    delete billData.idUser.role;
    delete billData.idUser.__v;

    res.status(200).json(bill);
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/user.controller => getBill] - ${error.message} - ${req.originalUrl} - ${req.method} - ${
        req.ip
      } - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const getBillToPdf = async (req: Request, res: Response) => {
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const _id = req.params.id_user;
  const idBill = req.params.id_bill as string;

  if (token._id !== _id) {
    return res.status(403).json({ error: "Non authentifié" });
  }
  if (!idBill) {
    return res.status(404).json({ error: "Facture introuvable" });
  }

  try {
    const bill: any = await Bill.findById(idBill)
      .populate({
        path: "idUser",
        model: "users",
      })
      .populate({
        path: "idSubscription",
        model: "subscriptions",
      });
    if (!bill) {
      return res.status(404).json({ error: "Facture introuvable" });
    }

    // ##########################################################
    //                  Génération de la facture
    // ##########################################################

    const createdAt = new Date(bill.createdAt).getTime();
    const month = moment(createdAt).format("MM");
    const year = moment(createdAt).format("YYYY");

    const billPdf = await generateBill(bill.idSubscription.price, bill.idSubscription.name, bill.idUser, bill.idStripe, createdAt);

    const attachments = [
      {
        filename: `IMIEVSCode_facture_${month}_${year}.pdf`,
        content: billPdf,
        encoding: "base64",
      },
    ];

    // ##########################################################
    //              Envoi du courriel avec le PDF
    // ##########################################################
    const message = messageEmail(
      bill.idUser.email,
      "Votre abonnement IMIE VSCode",
      "Votre abonnement a été modifié.",
      getBillTemplate(`${month}/${year}`),
      attachments
    );

    const mailSent = await sendMail(message);
    if (!mailSent) {
      return res.status(500).json({ error: "Erreur lors de l'envoi du courriel, veuillez réessayer plus tard" });
    }

    res.status(200).json({ message: "Un courriel vous a été envoyé avec votre facture en pièce jointe" });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/user.controller => getBillToPdf] - ${error.message} - ${req.originalUrl} - ${req.method} - ${
        req.ip
      } - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

//
// PARTIE ABONNEMENT UTILISATEUR
//

export const updateUserSubscription = async (req: Request, res: Response) => {
  const body = req.body as IUpdateUserSubscriptionRequestBody;
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const _id = req.params.id_user;

  if (token._id !== _id) {
    return res.status(403).json({ error: "Non authentifié" });
  }
  try {
    const userToUpdate: any = await User.findById(token._id);
    if (!userToUpdate) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }

    // on récupère l'abonnement voulu en base de données
    const subscription: any = await Subscription.findById(body.subscription_id);
    if (!subscription) {
      return res.status(404).json({ error: "Abonnement introuvable" });
    }

    // ##########################################################
    //                  Paiement avec Stripe
    // ##########################################################
    const card = {
      cardNumber: body.card_number,
      exp_month: body.exp.split("/")[0],
      exp_year: body.exp.split("/")[1],
      cvc: body.cvc,
    };
    let stripeSub: Stripe.Subscription | null = null;
    // on test si le paiement par Stripe fonctionne avec les informations de la carte bancaire de l'utilisateur
    // on ne fait pas la requête si le client choisit l'abonnement gratuit
    if (!/^gratuit$/i.test(subscription.name)) {
      try {
        stripeSub = await createSubscription(userToUpdate.email, card, subscription.name);
      } catch (error) {
        console.log("error: ", error);
        switch (error.raw.code as string) {
          case "incorrect_number":
            return res.status(400).json({ error: "Numéro de carte de crédit incorrect" });
          case "invalid_expiry_year":
            return res.status(400).json({ error: "Année d'expiration de la carte de crédit incorrecte" });
          case "invalid_expiry_month":
            return res.status(400).json({ error: "Mois d'expiration de la carte de crédit incorrect" });
          case "invalid_cvc":
            return res.status(400).json({ error: "CVC de la care de crédit incorrect" });
          default:
            return res.status(500).json({ error: "Erreur survenue lors du paiement, veuillez réessayer" });
        }
      }
    }

    // si l'id stripe n'existe pas, on générère un id numérique aléatoire
    if (!stripeSub?.id) {
      stripeSub = Object.assign(stripeSub, {
        id: randomstring.generate({
          length: 16,
          charset: "numeric",
        }),
      });
    }

    // ##########################################################
    //          Résiliation de l'ancien abonnement
    // ##########################################################

    // si l'abonnement actuel n'est pas gratuit et qu'un idStripe est renseigné (vérif), on doit se désabonner de l'ancien
    if (!/^gratuit$/i.test(subscription.name) && Boolean(userToUpdate.idStripe)) {
      try {
        await removeSubscription(userToUpdate.idStripe);
      } catch (error) {
        errorLogger.error(
          `${error.status || 500} - [src/controllers/user.controller => updateUserSubscription stripe] - ${error.message} - ${req.originalUrl} - ${
            req.method
          } - ${req.ip} - ${parseUserAgent(req)}`
        );
        console.log("error résiliation abonnement stripe: ", error);
      }
    }

    // ##########################################################
    //                  Génération de la facture
    // ##########################################################
    const subscribedSince = new Date();
    const month = moment(subscribedSince.getTime()).format("MM");
    const year = moment(subscribedSince.getTime()).format("YYYY");
    // header de la facture
    const billPdf = await generateBill(subscription.price, subscription.name, userToUpdate, stripeSub.id, subscribedSince.getTime());
    const attachments = [
      {
        filename: `IMIEVSCode_facture_${month}_${year}.pdf`,
        content: billPdf,
        encoding: "base64",
      },
    ];

    // ##########################################################
    //          Mise à jour de l'abonnement utilisateur
    // ##########################################################
    const update = {
      subscription: {
        name: subscription.name,
        description: subscription.description,
        price: subscription.price,
        advantages: subscription.advantages,
        subscribedSince: subscribedSince.toISOString(),
      },
      idStripe: stripeSub.id,
    };

    // sauvegarde des données de la facture en base de données ainsi que le nouvelle abonnement utilisateur
    const [userUpdated, bill] = await Promise.all([
      userToUpdate.set(update).save(),
      new Bill({
        idStripe: stripeSub.id,
        idUser: userToUpdate._id,
        idSubscription: subscription._id,
      }).save(),
    ]);
    // on formate les données de retour
    const user = JSON.parse(JSON.stringify(userUpdated));
    delete user.__v;
    delete user.password;
    delete user.role;
    delete user.active;

    // ##########################################################
    //              Envoi du courriel avec le PDF
    // ##########################################################
    const message = messageEmail(
      user.email,
      "Votre abonnement IMIE VSCode",
      "Votre abonnement a été modifié.",
      updateSubscriptionTemplate(user.lastname, user.firstname),
      attachments
    );

    sendMail(message);

    res.status(201).json({ message: "Votre abonnement a été mis à jour !", user });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/user.controller => updateUserSubscription] - ${error.message} - ${req.originalUrl} - ${
        req.method
      } - ${req.ip} - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const resetUserSubscription = async (req: Request, res: Response) => {
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const _id = req.params.id_user;

  if (token._id !== _id) {
    return res.status(403).json({ error: "Non authentifié" });
  }
  try {
    const userToUpdate: any = await User.findById(token._id);
    if (!userToUpdate) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }

    // on récupère le nom de l'abonnement que l'on veut résilier pour l'envoi du courriel
    const subscriptionToReset = userToUpdate.subscription.name as string;

    // on ajoute un abonnement gratuit par défault avec un regex qui prend le texte sans la casse, au cas où on change le nom
    // en récupérant que les champs nécessaires
    const subscription: any = await Subscription.findOne({ name: /^gratuit$/i }, { createdAt: 0, updatedAt: 0, _id: 0, __v: 0 });
    // si jamais l'abonnement gratuit n'est pas trouvé, ce qui ne doit jamais arriver
    if (!subscription) {
      return res.status(500).json({ error: "Erreur serveur" });
    }

    //si l'abonnement actuel n'est pas gratuit et qu'un idStripe est renseigné (vérif), on doit se désabonner de l'ancien
    if (!/^gratuit$/i.test(subscriptionToReset) && Boolean(userToUpdate.idStripe)) {
      try {
        await removeSubscription(userToUpdate.idStripe);
      } catch (error) {
        console.log("error: ", error);

        switch (error.raw.code as string) {
          case "resource_missing":
            return res.status(400).json({ error: "Abonnement déjà supprimé" });
          default:
            return res.status(500).json({ error: "Erreur survenue lors du paiement, veuillez réessayer" });
        }
      }
    }

    const update = {
      subscription: {
        name: subscription.name,
        description: subscription.description,
        price: subscription.price,
        advantages: subscription.advantages,
        subscribedSince: new Date(),
      },
      idStripe: null,
    };

    const userUpdated = await userToUpdate.set(update).save();
    // on formate les données de retour
    const user = JSON.parse(JSON.stringify(userUpdated));
    delete user.__v;
    delete user.password;
    delete user.role;
    delete user.active;

    // si l'utilisateur dispose déjà de l'abonnement gratuit, on évite d'envoyer un courriel inutile car il restera sur le même abonnement
    if (!/^gratuit$/i.test(subscriptionToReset)) {
      const message = messageEmail(
        user.email,
        "Votre abonnement IMIE VSCode",
        "Votre abonnement a été résilié.",
        resetSubscriptionTemplate(user.lastname, user.firstname, subscriptionToReset)
      );

      sendMail(message);
    }

    res.status(201).json({ message: "Votre abonnement a été résilié ! Vous recevrez un courriel de confirmation à la fin de votre échéance", user });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/user.controller => resetUserSubscription] - ${error.message} - ${req.originalUrl} - ${
        req.method
      } - ${req.ip} - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

//
// FONCTION POUR GENERER UNE FACTURE
//
const generateBill = async (price: string, subscriptionName: string, user: any, idStripe: string, subscribedSince: number) => {
  // header de la facture
  const header = `<div></div>`;

  // on calcul les prix
  const tva = (Number(price.replace(",", ".")) * 0.055).toFixed(2);
  const price_ht = (Number(price.replace(",", ".")) - Number(tva)).toFixed(2);

  // données qui vont hydrater le template html
  const pdfData = {
    subscription_price_ht: formatPrice(price_ht),
    tva_price: formatPrice(tva),
    total_price: price,
    subscription_name: subscriptionName,
    date_creation: moment(subscribedSince).format("LL"),
    id_facture: idStripe,
    adresse: `${user.numAddress} ${user.address},${user.city}, ${user.postalCode}`,
    pays: user.country,
  };

  // option du pdf
  const options = {
    format: "a4" as puppeteer.PaperFormat,
    landscape: true,
    margin: {
      left: 25,
      right: 25,
      bottom: 40,
      top: 5,
    },
  };

  // on génère la facture
  const pdfGenerator = new PdfGenerator(pdfData, "src/utils/pdf-templates/FactureTemplate.html", header, true, options);
  // on transforme le pdf en base64
  const buffer = await pdfGenerator.generate();

  return buffer as Buffer;
};

//
// PROJETS
//
export const newProject = async (req: Request, res: Response) => {
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const body = req.body as INewProjectRequestBody;
  const _id = req.params.id_user;
  const projectPath = `${process.env.BASE_PATH}projects/${_id}`;

  if (token._id !== _id) {
    return res.status(403).json({ error: "Non authentifié" });
  }
  try {
    const user: any = await User.findById(token._id);
    if (!user) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }

    // si l'utilisateur a un abonnement gratuit, on vérifie son nombre de projets max
    if (/^gratuit$/i.test(user.subscription.name)) {
      // on récupère les projets
      const projects = await Project.find({ idUser: user._id }, { group: 0, idUser: 0 });
      if (projects.length >= 1) {
        return res.status(400).json({
          error:
            "Vous avez atteint le quota maximum de 1 projet. Mettez à jour votre abonnement pour bénéficier de la création de projets supplémentaires",
        });
      }
    }

    // on vérifie que le projet n'existe pas chez l'utilisateur
    const projectexist = await Project.findOne({ idUser: _id, name: body.name }).populate({
      path: "idUser",
      model: "users",
    });
    if (projectexist) {
      return res.status(400).json({ error: "Projet déjà existant. Veuillez choisir un autre nom" });
    }

    // on véirfie si le dossier utilisateur existe
    if (!fs.existsSync(projectPath)) {
      fs.mkdirSync(projectPath, { recursive: true });
    }

    let entry = "";

    // on remplace les espaces par des tirets
    const name = body.name.split(" ").join("-").toLowerCase();

    // on vérifie que le dossier utilisateur existe
    const pathExist = fs.existsSync(projectPath);
    if (!pathExist) {
      fs.mkdirSync(projectPath);
    }

    // ##########################################################
    // Vérification si le dossier du nouveau projet que l'on veut créer n'existe pas
    // ##########################################################
    const projectExist = fs.existsSync(`${projectPath}/${name}`);
    if (projectExist) {
      return res.status(400).json({ error: "Projet déjà existant. Veuillez choisir un autre nom" });
    }

    // ##########################################################
    //                   Choix du template
    // ##########################################################
    const projectTemplates = `${process.env.BASE_PATH}project-templates`;

    // fonction pour déplacer le contenu du template choisi
    const cpTemplate = (src: string, dest: string) => {
      // on copie le contenu du template choisi vers le répertoire utilisateur
      fs.copySync(src, dest, { recursive: true });

      // On récupère le package JSON par rapport au template pour modifier le nom du projet
      const packagejson = fs.readJSONSync(`${dest}/package.json`);
      packagejson.name = name;
      // on remplace le contenu du package.json
      fs.writeJSONSync(`${dest}/package.json`, packagejson, { flag: "w" });
    };

    switch (body.template) {
      case "REACT":
        cpTemplate(`${projectTemplates}/react-template`, `${projectPath}/${name}`);
        entry = `projects/${_id}/${name}/src/index.js`;
        break;
      case "ANGULAR":
        cpTemplate(`${projectTemplates}/angular-template`, `${projectPath}/${name}`);
        entry = `projects/${_id}/${name}/src/main.ts`;
        break;
      case "VUE":
        cpTemplate(`${projectTemplates}/vue-template`, `${projectPath}/${name}`);
        entry = `projects/${_id}/${name}/src/main.js`;
        break;
      default:
        cpTemplate(`${projectTemplates}/react-template`, `${projectPath}/${name}`);
        entry = `projects/${_id}/${name}/src/index.js`;
        break;
    }

    // quand la création se temrine, on sauvegarde les infos du projet en base de données
    await new Project({
      name: body.name,
      template: body.template,
      path: `projects/${_id}/${name}`,
      entry,
      idUser: _id,
    }).save();

    res.status(201).json({ message: "Projet créé" });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/user.controller => newProject] - ${error.message} - ${req.originalUrl} - ${req.method} - ${
        req.ip
      } - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const listAllProjects = async (req: Request, res: Response) => {
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const _id = req.params.id_user;

  if (token._id !== _id) {
    return res.status(403).json({ error: "Non authentifié" });
  }
  try {
    const user: any = await User.findById(token._id);
    if (!user) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }

    // on vérifie que le projet n'existe pas chez l'utilisateur
    const projects = await Project.find({ $or: [{ idUser: _id }, { group: _id }] }, { __v: 0, group: 0 }).populate({
      path: "group",
      model: "users",
    });

    const data = JSON.parse(JSON.stringify(projects));
    for (const project of data) {
      // si la personne est le créateur du projet
      project.masterOfProject = project.idUser === _id ? true : false;
      delete project.idUser;
    }

    res.status(200).json(data);
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/user.controller => listAllProjects] - ${error.message} - ${req.originalUrl} - ${req.method} - ${
        req.ip
      } - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const getRootProject = async (req: Request, res: Response) => {
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const id_user = req.params.id_user;
  const id_project = req.params.id_project;

  if (token._id !== id_user) {
    return res.status(403).json({ error: "Non authentifié" });
  }
  try {
    const user: any = await User.findById(token._id);
    if (!user) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }

    // ##########################################################
    //              Vérification si le projet existe
    // ##########################################################
    const project: any = await Project.findOne(
      { $or: [{ idUser: id_user }, { group: id_user }], _id: id_project },
      { __v: 0, idUser: 0, group: 0 }
    ).populate({
      path: "group",
      model: "users",
    });
    if (!project) {
      return res.status(404).json({ error: "Projet introuvable" });
    }

    // ##########################################################
    //              Liste des fichiers/dossiers
    // ##########################################################
    const fileNames = [],
      dirNames = [];

    // on récupère les dossiers/fichiers
    const projectPath = `${process.env.BASE_PATH}${project.path}`;
    const files = fs.readdirSync(projectPath);
    for (const file of files) {
      // on envoie que les noms de dossiers ou des fichiers
      if (file !== "node_modules") fs.lstatSync(`${projectPath}/${file}`).isDirectory() ? dirNames.push(file) : fileNames.push(file);
    }

    // on récupère le contenu du fichier principal
    const entryPath = `${process.env.BASE_PATH}${project.entry}`;
    const entry = fs.readFileSync(entryPath, { encoding: "utf8" });

    // on récupère tous les chemins des fichiers sauf les node_modules
    const tree = dirTree(projectPath, { exclude: /node_modules/ });

    res.status(200).json({ files: fileNames, dirs: dirNames, entry, entryPath, path: project.path, tree: tree.children });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/user.controller => getRootProject] - ${error.message} - ${req.originalUrl} - ${req.method} - ${
        req.ip
      } - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const addUserInProject = async (req: Request, res: Response) => {
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const body = req.body as IAddUserInProjectRequestBody;
  const _id = req.params.id_user;
  const id_project = req.params.id_project;

  if (token._id !== _id) {
    return res.status(403).json({ error: "Non authentifié" });
  }
  try {
    const user: any = await User.findById(token._id);
    if (!user) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }

    // ##############################################################
    // on récupère l'utilisateur que l'on souhaite ajouter
    // ##############################################################
    const userToInsertInGroup = await User.findById(body.id_user);
    if (!userToInsertInGroup) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }

    // ##########################################################
    //              Vérification si le projet existe
    // ##########################################################
    const project: any = await Project.findOne({ idUser: _id, _id: id_project }, { __v: 0, idUser: 0 });
    if (!project) {
      return res.status(404).json({ error: "Projet introuvable" });
    }

    // ##########################################################
    //              Ajout de l'utilisateur au projet
    // ##########################################################
    const group = project.group;
    for (const gr of group) {
      if (String(gr) === String(body.id_user)) {
        return res.status(400).json({ message: "Utilisateur déjà présent dans le projet" });
      }
    }

    // on vérifie que l'utilisateur n'est pas dans le projet
    group.push(body.id_user);
    project.group = group;
    await project.save();
    res.status(201).json({ message: "Utilisateur ajouté au projet !" });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/user.controller => addUserInProject] - ${error.message} - ${req.originalUrl} - ${req.method} - ${
        req.ip
      } - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const removeUserInProject = async (req: Request, res: Response) => {
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const _id = req.params.id_user;
  const id_project = req.params.id_project;
  // utilisateur que l'on veut supprimer
  const query = req.query as { id_user: string };

  if (token._id !== _id) {
    return res.status(403).json({ error: "Non authentifié" });
  }
  try {
    const user: any = await User.findById(token._id);
    if (!user) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }

    // on vérifie que le projet n'existe pas chez l'utilisateur
    const project: any = await Project.findOne({ $and: [{ idUser: _id }, { _id: id_project }] }, { __v: 0 });
    if (!project) {
      return res.status(404).json({ error: "Projet introuvable" });
    }

    const group = JSON.parse(JSON.stringify(project.group));
    for (let i = 0; i < project.group.length; i++) {
      const user = group[i];
      if (String(user) === String(query.id_user)) {
        group.splice(i, 1);
      }
    }

    project.group = group;
    await project.save();

    res.status(201).json({ message: "Utilisateur supprimé du projet !" });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/user.controller => removeUserInProject] - ${error.message} - ${req.originalUrl} - ${req.method} - ${
        req.ip
      } - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const removeProject = async (req: Request, res: Response) => {
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const _id = req.params.id_user;
  const id_project = req.params.id_project;

  if (token._id !== _id) {
    return res.status(403).json({ error: "Non authentifié" });
  }
  try {
    const user: any = await User.findById(token._id);
    if (!user) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }

    // on vérifie que le projet n'existe pas chez l'utilisateur
    const project: any = await Project.findOne({ $and: [{ idUser: _id }, { _id: id_project }] }, { __v: 0 });
    if (!project) {
      return res.status(404).json({ error: "Projet introuvable" });
    }

    // ##########################################################
    //         On supprime le dossier contenant le projet
    // ##########################################################
    const projectPath = `${process.env.BASE_PATH}${project.path}`;
    if (fs.existsSync(projectPath)) {
      fs.rmSync(projectPath, { force: true, recursive: true });
    }

    // ##########################################################
    //              On supprime le projet en BDD
    // ##########################################################
    await project.remove();
    res.status(201).json({ message: "Projet supprimé !" });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/user.controller => removeProject] - ${error.message} - ${req.originalUrl} - ${req.method} - ${
        req.ip
      } - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

export const leaveProject = async (req: Request, res: Response) => {
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const _id = req.params.id_user;
  const id_project = req.params.id_project;

  if (token._id !== _id) {
    return res.status(403).json({ error: "Non authentifié" });
  }
  try {
    const user: any = await User.findById(token._id);
    if (!user) {
      return res.status(404).json({ error: "Utilisateur introuvable" });
    }

    // on vérifie que le projet n'existe pas chez l'utilisateur
    const project: any = await Project.findOne({ $and: [{ group: _id }, { _id: id_project }] }, { __v: 0 });
    if (!project) {
      return res.status(404).json({ error: "Projet introuvable" });
    }

    // ##########################################################
    //            On supprime l'utilisateur du projet
    // ##########################################################
    const group = JSON.parse(JSON.stringify(project.group));
    for (let i = 0; i < project.group.length; i++) {
      const element = project.group[i];
      if (String(element) === _id) {
        group.splice(i, 1);
        break;
      }
    }

    // mise à jour des utilisateurs du groupe
    project.group = group;
    await project.save();

    res.status(201).json({ message: `Vous venez de quitter le projet ${project.name}` });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/user.controller => leaveProject] - ${error.message} - ${req.originalUrl} - ${req.method} - ${
        req.ip
      } - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

//
// RECHERCHER DES UTILISATEURS
//
export const searchUsers = async (req: Request, res: Response) => {
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const query = req.query as { q: string };
  const _id = req.params.id_user;
  const id_project = req.params.id_project;

  try {
    // ##########################################################
    //              On récupère le projet
    // ##########################################################
    const project: any = await Project.findOne(
      { idUser: _id, _id: id_project },
      { __v: 0, idUser: 0, path: 0, entry: 0, createdAt: 0, updatedAt: 0, name: 0, template: 0 }
    );
    if (!project) {
      return res.status(404).json({ error: "Projet introuvable" });
    }

    // ##########################################################
    // On récupère tous les utilisateurs sauf l'utilisateur courrant et ceux déjà présent dans le projet
    // ##########################################################
    const users = await User.find(
      {
        $or: [
          { pseudo: { $regex: `^${query.q}`, $options: "i" } },
          { email: { $regex: `^${query.q}`, $options: "i" } },
          { firstname: { $regex: `^${query.q}`, $options: "i" } },
          { lastname: { $regex: `^${query.q}`, $options: "i" } },
        ],
        $and: [
          {
            email: { $ne: token.email },
            _id: { $nin: project.group },
          },
        ],
      },
      {
        role: 0,
        active: 0,
        password: 0,
        __v: 0,
        subscription: 0,
        facebookId: 0,
        googleId: 0,
        githubId: 0,
        idStripe: 0,
        address: 0,
        postalCode: 0,
        numberAddress: 0,
        city: 0,
      }
    );

    res.status(200).json(users);
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/user.controller => searchUsers] - ${error.message} - ${req.originalUrl} - ${req.method} - ${
        req.ip
      } - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

//
// STATS UTILISATEURS
//
export const getStats = async (req: Request, res: Response) => {
  const bearer = req.headers.authorization as string;
  const token = getToken(bearer) as IToken;
  const _id = req.params.id_user;

  try {
    // ##########################################################
    // On récupère les stats et les projets de l'utilisateur
    // ##########################################################
    const [stats, projects]: [any[], any[]] = await Promise.all([
      Stats.find({ idUser: token._id }, { idUser: 0, __v: 0 }, { sort: { createdAt: "ASC" } }).populate({
        path: "streamSessions",
        model: "sessions",
      }),
      Project.find({ $or: [{ idUser: _id }, { group: _id }] }, { __v: 0, group: 0 }),
    ]);

    // ##########################################################
    // On récupère le nom entier des langages en incrémentant le nombre de fichiers avec la même extension
    // ##########################################################
    const dataProjects = [];
    for (const project of projects) {
      // on récupère tous les chemins des fichiers sauf les node_modules
      const tree = dirTree(`${process.env.BASE_PATH}${project.path}`, { exclude: /node_modules/ });

      // on récupère les langages
      const languages = recupererTousLesLangages(tree?.children || [], []);
      const languagesPurifies = {} as { [key: string]: number };

      languages.filter(Boolean).forEach(lang => {
        // si l'extension existe
        if (Object.keys(EXTENSIONS).find(key => key === lang)) {
          // on récupère la valeur propre de l'extension (nom du langage)
          const value = EXTENSIONS[lang];
          // si le langage existe on incrémente sinon on l'initialise
          languagesPurifies[value] = languagesPurifies[value] ? languagesPurifies[value] + 1 : 1;
        } else {
          // on stocke tous les autres fichiers dans une autre clé
          languagesPurifies["Autres"] = languagesPurifies["Autres"] ? (languagesPurifies["Autres"] = languagesPurifies["Autres"] + 1) : 1;
        }
      });

      // ##########################################################
      //                On calcul le pourcentage
      // ##########################################################
      const pourcentage = {} as { [key: string]: number };
      Object.entries(languagesPurifies).forEach(entry => {
        const [key, value] = entry;
        pourcentage[key] = value;
      });

      // ajout des valeurs
      dataProjects.push({
        numberOfFiles: languages.length,
        languages: pourcentage,
        name: project.name,
        idProject: project._id,
        template: project.template,
      });
    }

    const data = JSON.parse(JSON.stringify(stats));
    for (const stat of data) {
      // ##########################################################
      //                On calcul la durée des sessions
      // ##########################################################
      // on récupère que les sessions terminées donc hors ligne
      stat.streamSessions = stat.streamSessions
        .filter(session => !session.online)
        .map(session => {
          return {
            uuid: session.uuid,
            idProject: session.idProject,
            time: session.time,
            createdAt: session.createdAt,
          };
        });
    }

    res.status(200).json({ sessions: data, projects: dataProjects });
  } catch (error) {
    console.log("error: ", error);
    errorLogger.error(
      `${error.status || 500} - [src/controllers/user.controller => getStats] - ${error.message} - ${req.originalUrl} - ${req.method} - ${
        req.ip
      } - ${parseUserAgent(req)}`
    );
    res.status(500).json({ error: "Erreur serveur" });
  }
};

const recupererTousLesLangages = (trees: any[], languages: string[]) => {
  trees.forEach(tree => {
    if (tree.type === "directory") {
      recupererTousLesLangages(tree.children, languages);
    } else {
      languages.push(tree.extension);
    }
  });

  return languages;
};
