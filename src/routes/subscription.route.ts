import express from "express";
import {
  createSubscription,
  updateSubscription,
  deleteSubscription,
  getAllSubscriptions,
  getSubscription,
} from "../controllers/subscription.controller";
import { createSubscriptionMiddleware } from "../middlewares/subscription.middleware";
const router = express.Router();

/**
 * GET
 */
router.get("/", getAllSubscriptions);
router.get("/:id_subscription", getSubscription);

/**
 * POST
 */
router.post("/", createSubscriptionMiddleware, createSubscription);

/**
 * PUT
 */
router.put("/:id_subscription", createSubscriptionMiddleware, updateSubscription);

/**
 * DELETE
 */
router.delete("/:id_subscription", deleteSubscription);

export default router;
