import express from "express";
const router = express.Router();
import auth from "./auth.route";
import user from "./user.route";
import subscription from "./subscription.route";
import { userMiddleware } from "../middlewares/user.middleware";
import { employeMiddleware } from "../middlewares/admin/employe.middleware";
import admin from "./admin/index";
/**
 * PARTIE PUBLIC
 */
router.use("/api/auth", auth);
router.use("/api/user", userMiddleware, user);
router.use("/api/subscription", subscription);

/**
 * PARTIE PRIVE
 */
router.use("/api/auth", auth);
router.use("/api/admin", employeMiddleware, admin);

export default router;
