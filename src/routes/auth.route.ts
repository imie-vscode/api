import express from "express";
import { registerWithAPIMiddleware } from "../middlewares/auth.middleware";
import { registerWithGithubController } from "../controllers/auth.controller";
import {
  registerWithGoogleController,
  loginWithGoogleController,
  registerWithFacebookController,
  loginWithFacebookController,
} from "../controllers/auth.controller";
import {
  registerController,
  loginController,
  forgotPasswordController,
  resetPasswordController,
  loginWithGithubController,
} from "../controllers/auth.controller";
import { registerMiddleware, loginMiddleware, forgotPasswordMiddleware, resetPasswordMiddleware } from "../middlewares/auth.middleware";
const router = express.Router();

/**
 * GET
 */

/**
 * POST
 */
router.post("/login", loginMiddleware, loginController);
router.post("/login/google", loginWithGoogleController);
router.post("/login/facebook", loginWithFacebookController);
router.post("/login/github", loginWithGithubController);

router.post("/register", registerMiddleware, registerController);
router.post("/facebook", registerWithAPIMiddleware, registerWithFacebookController);
router.post("/google", registerWithAPIMiddleware, registerWithGoogleController);
router.post("/github", registerWithAPIMiddleware, registerWithGithubController);

router.post("/forgot-password", forgotPasswordMiddleware, forgotPasswordController);

/**
 * PUT
 */
router.put("/reset-password", resetPasswordMiddleware, resetPasswordController);

export default router;
