import express from "express";
import {
  getProfile,
  updateProfile,
  removeProfile,
  getpersonnalData,
  updateUserSubscription,
  resetUserSubscription,
  newProject,
  listAllProjects,
  getRootProject,
  addUserInProject,
  searchUsers,
  removeUserInProject,
  removeProject,
  leaveProject,
  getStats,
} from "../controllers/user.controller";
import {
  updateUserMiddleware,
  updateUserSubscriptionMiddleware,
  newprojectMiddleware,
  addUserInProjectMiddleware,
} from "../middlewares/user.middleware";
import { getBills, getBillToPdf, getBill } from "../controllers/user.controller";
const router = express.Router();

/**
 * GET
 */
router.get("/:id_user", getProfile);
router.get("/:id_user/personnal-data", getpersonnalData);
router.get("/:id_user/bill", getBills);
router.get("/:id_user/bill/:id_bill", getBill);
router.get("/:id_user/bill/:id_bill/pdf", getBillToPdf);
router.get("/:id_user/project", listAllProjects);
router.get("/:id_user/project/:id_project/root", getRootProject);
router.get("/:id_user/project/:id_project/search", searchUsers);
router.get("/:id_user/stats", getStats);

/**
 * POST
 */
router.post("/:id_user/project/:id_project/add-user", addUserInProjectMiddleware, addUserInProject);
router.post("/:id_user/project", newprojectMiddleware, newProject);

/**
 * PUT
 */
router.put("/:id_user", updateUserMiddleware, updateProfile);
router.put("/:id_user/subscription", updateUserSubscriptionMiddleware, updateUserSubscription);

/**
 * DELETE
 */
router.delete("/:id_user", removeProfile);
router.delete("/:id_user/subscription", resetUserSubscription);
router.delete("/:id_user/project/:id_project/delete-user", removeUserInProject);
router.delete("/:id_user/project/:id_project/delete-project", removeProject);
router.delete("/:id_user/project/:id_project/leave-project", leaveProject);

export default router;
