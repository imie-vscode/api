import express from "express";
import { employeMiddleware } from "../../middlewares/admin/employe.middleware";
const router = express.Router();
import adminAuth from "./admin.auth.route"


/**
 * PARTIE PRIVE
 */
 router.use("/auth", adminAuth );
 router.use("/employe", adminAuth );


export default router;





