import express from "express";
import { adminRemoveProfile, adminUpdateProfile } from "../../controllers/admin/employe.controller";
import { getProfile } from "../../controllers/user.controller";
import { updateEmployeMiddleware } from "../../middlewares/admin/employe.middleware";
const router = express.Router();

/**
 * GET
 */
 router.get("/:id_user", getProfile);
 /**
 * POST
 */

/**
 * PUT
 */
 router.put("/:id_user", updateEmployeMiddleware, adminUpdateProfile);


/**
 * DELETE
 */
 router.delete("/:id_user", adminRemoveProfile);






