import express from "express";
import { adminRegisterController } from "../../controllers/admin/admin.auth.controller";
import { adminUpdateProfile } from "../../controllers/admin/employe.controller";
import { adminRegisterMiddleware } from "../../middlewares/admin/admin.auth.middleware";
const router = express.Router();

/**
 * GET
 */

/**
 * POST
 */
router.post("/register", adminRegisterMiddleware, adminRegisterController);
router.post("/update", adminUpdateProfile, )


export default router;
