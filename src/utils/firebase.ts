import { parse } from "dotenv/types";
import admin from "firebase-admin";
import fs from "fs";
const serviceAccount = fs.readFileSync("imie-vscode-firebase.json", { encoding: "utf-8" });

admin.initializeApp({
  credential: admin.credential.cert(JSON.parse(serviceAccount)),
  storageBucket: "imie-vscode.appspot.com",
});

export const bucket = admin.storage().bucket();
