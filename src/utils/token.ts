import jwt from "jsonwebtoken";
import fs from "fs";
import { errorLogger } from "../config/winston";
require("dotenv").config();
interface ICreateToken {
  email: string;
  role: string;
  firstname: string;
  lastname: string;
  _id: string;
}

export interface IToken extends ICreateToken {
  sub: string;
}

export const createToken = ({ email, role, firstname, lastname, _id }: ICreateToken) => {
  // sign with RSA SHA256
  try {
    return jwt.sign(
      {
        sub: String(Math.sqrt(Math.pow(Math.PI, Math.exp(Math.PI)))),
        email,
        role,
        firstname,
        lastname,
        _id,
      },
      process.env.PRIVATE_KEY,
      {
        expiresIn: "24h",
      }
    );
  } catch (error) {
    errorLogger.error(`500 - [utils/token => createToken] - Erreur lors la génération du token`);
    console.log("error: ", error);
    return false;
  }
};

export const createTokenTest = ({ email, role, firstname, lastname, _id }: ICreateToken) => {
  try {
    return jwt.sign(
      {
        sub: String(Math.sqrt(Math.pow(Math.PI, Math.exp(Math.PI)))),
        email,
        role,
        firstname,
        lastname,
        _id,
      },
      "test",
      {
        expiresIn: "24h",
      }
    );
  } catch (error) {
    errorLogger.error(`500 - [utils/token => createToken] - Erreur lors la génération du token`);
    console.log("error: ", error);
    return false;
  }
};

export const getToken = (bearer: string): false | IToken => {
  const token = bearer.split("Bearer ")[1];
  try {
    return jwt.verify(token, process.env.PRIVATE_KEY) as IToken;
  } catch (error) {
    errorLogger.error(`500 - [utils/token => getToken] - Erreur lors la récupération du token`);
    return false;
  }
};

export const getTokenTest = (bearer: string): false | IToken => {
  const token = bearer.split("Bearer ")[1];
  try {
    return jwt.verify(token, "test") as IToken;
  } catch (error) {
    errorLogger.error(`500 - [utils/token => getToken] - Erreur lors la récupération du token`);
    return false;
  }
};
