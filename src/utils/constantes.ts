export const ROLES = ["ADMIN", "CLIENT", "EMPLOYE"];
export const SEXE = ["Homme", "Femme"];
export const PRIVATES_ROLES_ALLOWED = ["ADMIN", "EMPLOYE"];

export const TEMPLATES = ["REACT", "ANGULAR", "VUE"];

export const EXTENSIONS = {
  ".html": "HTML",
  ".js": "JavaScript",
  ".jsx": "JavaScript",
  ".css": "CSS",
  ".sql": "SQL",
  ".php": "PHP",
  ".ts": "TypeScript",
  ".tsx": "TypeScript",
  ".json": "JSON",
  ".xml": "XML",
};

export const FAKE_EMAIL = "adri_00@hotmail.fr";
