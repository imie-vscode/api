import path from "path";
import fs from "fs";
import puppeteer from "puppeteer";
import hb from "handlebars";
import { errorLogger } from "../config/winston";

/**
 * format : a4 par défaut |
 * landscape : false par défaut |
 * margin-bottom : 80 par défaut |
 * margin-right : 25 par défaut |
 * margin-left : 25 par défaut |
 * margin-top : 80 par défaut |
 */
interface IOptions {
  format?: puppeteer.PaperFormat;
  landscape?: boolean;
  margin?: {
    bottom: number | string;
    left: number | string;
    right: number | string;
    top: number | string;
  };
}

export default class PdfGenerator {
  private _data: { [key: string]: any };
  private _templatePath: string;
  private _headerTemplate: string;
  private _footerTemplate: boolean;
  private _options?: IOptions | undefined;

  constructor(data: { [key: string]: any }, templatePath: string, headerTemplate: string, footerTemplate: boolean, options?: IOptions) {
    this._data = data;
    this._templatePath = templatePath;
    this._headerTemplate = headerTemplate;
    this._footerTemplate = footerTemplate;
    this._options = options;
  }

  private get templateHtml() {
    try {
      const invoicePath = path.resolve(this._templatePath);
      return fs.readFileSync(invoicePath, "utf8");
    } catch (err) {
      errorLogger.error(`500 - [utils/PdfGenerator => templateHtml] - Erreur lors la récupération du template ${this._templatePath}`);
      return false;
    }
  }

  public async generate() {
    const footer = `<div style="font-size: 10px;color:silver; width: 100%; height: 25px;text-align:center;">IMIE VSCode - Adrien MAILLARD - Fatima Elhasbi - Joël Litete</div>`;

    const content = this.templateHtml;
    // si y'a une erreur dans la récupération du template html sur le serveur
    if (!content) return false;

    const template = hb.compile(content, { data: true });
    const html = template(this._data);
    // lancement de l'émulation d'une page web
    const browser = await puppeteer.launch();
    // lancement de la page
    const page = await browser.newPage();
    // hydratation des données dans la page
    await page.setContent(html);
    // génération du pdf
    const buffer = await page.pdf({
      format: this._options?.format ? this._options.format : "a4",
      landscape: this._options?.landscape ? this._options.landscape : false,
      margin: {
        bottom: this._options?.margin?.bottom ? this._options.margin.bottom : 95,
        left: this._options?.margin?.left ? this._options.margin.left : 25,
        right: this._options?.margin?.right ? this._options.margin.right : 25,
        top: this._options?.margin?.top ? this._options.margin.top : 95,
      },
      headerTemplate: this._headerTemplate,
      footerTemplate: footer,
      displayHeaderFooter: true,
    });
    // fermeture de la page
    await browser.close();
    return buffer;
  }
}
